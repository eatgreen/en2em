   % ======================================================================
   %                       ***  Driver EnVar  ***
   % 4DENVar : Main driver for EnVar
   %======================================================================
   
close all
clear all
clc

RootDir=pwd;

addpath([RootDir,'/observation/'])
addpath([RootDir,'/model/'])
addpath([RootDir,'/ensemble'])
addpath([RootDir,'/localization'])
addpath([RootDir,'/analysis/'])
addpath([RootDir,'/optimization/'])
addpath([RootDir,'/tools/'])
addpath([RootDir,'/result/'])
addpath([RootDir,'/parameters/'])

Obs_Type='Syn2D';
IC='Wave';

disp(['This is a ',Obs_Type,' test with IC ',IC]);
par=par_config(Obs_Type,IC);

if strcmp(par.restart_flag,'Off')
    assert(par.restart_cycle==1);
else
    assert(par.restart_cycle~=1);
    assert(par.restart_cycle<=par.Assimilation_cycle);
end

dym=DYM(par);
das=DAS(par);
loc=LOC(par,das,dym);
ens=ENS(par);
opt=OPT(par);
      
% Root path for large data
switch par.Obs_Type
    case {'Syn2D'}
    RootPathLD='/Users/yang/Ensemble/large_data/';
    switch das.Algo_Localization_Type
        case 'None'
            switch das.Algo_EnsembleUpdate_Type
                case 'Ensemble_Analysis'
                ResultDir=strcat('Result/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'S_codens',num2str(ens.codENS),'_NoLocal','_toffset',num2str(das.t_offset),'_AssimCycle',num2str(das.Assimilation_cycle),ens.Inf_suffix);
                RestartDir=strcat('restartfiles/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'S_codens',num2str(ens.codENS),'_NoLocal','_toffset',num2str(das.t_offset),'_AssimCycleTot',num2str(das.Assimilation_cycle_tot),ens.Inf_suffix);
                case 'DEn_Analysis'
                ResultDir=strcat('Result/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'D_codens',num2str(ens.codENS),'_NoLocal','_toffset',num2str(das.t_offset),'_AssimCycle',num2str(das.Assimilation_cycle),ens.Inf_suffix);
                RestartDir=strcat('restartfiles/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'D_codens',num2str(ens.codENS),'_NoLocal','_toffset',num2str(das.t_offset),'_AssimCycleTot',num2str(das.Assimilation_cycle_tot),ens.Inf_suffix);
            end
        case 'Localize_Covariance'
            switch das.Algo_EnsembleUpdate_Type
                case 'Ensemble_Analysis'
                ResultDir=strcat('Result/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'S_codens',num2str(ens.codENS),'_cod',num2str(loc.cod),'_r',num2str(loc.r),'_toffset',num2str(das.t_offset),'_AssimCycle',num2str(das.Assimilation_cycle),ens.Inf_suffix);
                RestartDir=strcat('restartfiles/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'S_codens',num2str(ens.codENS),'_cod',num2str(loc.cod),'_r',num2str(loc.r),'_toffset',num2str(das.t_offset),'_AssimCycleTot',num2str(das.Assimilation_cycle_tot),ens.Inf_suffix);
                case 'DEn_Analysis'
                ResultDir=strcat('Result/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'D_codens',num2str(ens.codENS),'_cod',num2str(loc.cod),'_r',num2str(loc.r),'_toffset',num2str(das.t_offset),'_AssimCycle',num2str(das.Assimilation_cycle),ens.Inf_suffix);
                RestartDir=strcat('restartfiles/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'D_codens',num2str(ens.codENS),'_cod',num2str(loc.cod),'_r',num2str(loc.r),'_toffset',num2str(das.t_offset),'_AssimCycleTot',num2str(das.Assimilation_cycle_tot),ens.Inf_suffix);               
            end
        case 'Local_Analysis'
            ResultDir=strcat('Result/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'T_codens',num2str(ens.codENS),'_locx',num2str(loc.LocalSizex),'_locy',num2str(loc.LocalSizey),'_toffset',num2str(das.t_offset),'_AssimCycle',num2str(das.Assimilation_cycle),ens.Inf_suffix);
            RestartDir=strcat('restartfiles/obs',das.Assimflag_suffix,'_nxobs',num2str(dym.nxobs),'_nx',num2str(dym.nx),'_dt',num2str(dym.dt),'_it',num2str(opt.MaxIter_Outer),'_m',num2str(das.nobs),'_N',num2str(ens.nens),'T_codens',num2str(ens.codENS),'_locx',num2str(loc.LocalSizex),'_locy',num2str(loc.LocalSizey),'_toffset',num2str(das.t_offset),'_AssimCycleTot',num2str(das.Assimilation_cycle_tot),ens.Inf_suffix);
    end
end
tf=isdir(ResultDir);
if tf==0
mkdir(ResultDir);
else
%askcontinue=input('The same configuration directory already exists, continue will erase results, continue?');
end

tr=isdir(RestartDir);
if tr==0
mkdir(RestartDir);
end

obs=OBS(par);
if strcmp(das.restart_flag,'Off')
obs=obs.make_synth(das,ens);
else
obs=obs.load_restartfiles(das);
end

disp(['Forecast    run on ',num2str(obs.nx),' * ',num2str(obs.ny)]);
disp(['Observation run on ',num2str(obs.nxobs),' * ',num2str(obs.nyobs)]);
disp(['Assimilate ',das.Assimilation_flag,' state variable(s)']);
disp(['Estimate parameters : ',das.ParameterEstimation_flag]);
disp(['Ensemble number     : ',num2str(ens.nens)]);
disp(['Analysis scheme     : ',das.Algo_EnsembleUpdate_Type]);
disp(['Ensemble inflation  : ',ens.EnsembleInflation_flag]);
disp(['Localization scheme : ',das.Algo_Localization_Type]);
disp(['Cutoff distance     : ',num2str(loc.cod)]);

for CycleIndex=das.restart_cycle:das.Assimilation_cycle
    disp(['Working on Cycle ',num2str(CycleIndex),' of Assimilation cycle ',num2str(das.Assimilation_cycle)]);
    das.CycleIndex=CycleIndex;
    res_cycle(CycleIndex)=RES();
    
    if strcmp(das.restart_flag,'On')
       %load restart files
       for resIndex=CycleIndex:-1:2
           res_var=load(strcat(RestartDir,'/rst_cycle',num2str(resIndex-1),'.mat'));
           res_cycle(resIndex-1)=res_var.res;
       end
    end
    
    das=das.BackgroundState(obs,res_cycle);
    
    das=das.InitialState(obs,res_cycle);
    
    if das.CycleIndex==1    
    ens=ens.EnsembleInitial(obs,das);
    end
    
    for iter=1:opt.MaxIter_Outer
        opt.Iter_Outer=iter;
        disp(['Working on loop ',num2str(iter),' of Outer loop ',num2str(opt.MaxIter_Outer)]);   
        opt=opt.OptimisationOuter(obs,das,ens,loc,res_cycle);
    end
    
    %ensemble analysis to ensemble forecast for next cycle
    if das.Assimilation_cycle_tot~=1 && CycleIndex~=das.Assimilation_cycle_tot
    ens=ens.EnsembleIntegration(obs,das);
    end

    %calculate analysis
    das=das.AnalysisState(obs);

    %calculate rms
    das=das.CalculateRMS(obs);

    %plot rms single cycle
%     das=das.PlotRMS(obs,ens,opt,loc,ResultDir);

    %save results
    das=das.SaveResult(obs,ens,res_cycle,RestartDir);

    %reset das for next cycle
    das=das.ResetforNextCycle;
    
end

das=das.ForecastAndPlot(obs,res_cycle,ResultDir);
%  das=das.PlotRMSAll(obs,res_cycle,ResultDir);
