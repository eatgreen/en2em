   % ======================================================================
   %                       ***  Class DAS  ***
   % 4DENVar : Generic assimilation class 
   %======================================================================

   %----------------------------------------------------------------------
   %    DAS              : Constructor 
   %    BackgroundState   
   %    InitialState      
   %    ForecastState     
   %    AnalysisState    
   %    CalculateRMS
   %    PlotRMS
   %    PlotRMSAll
   %----------------------------------------------------------------------
   
classdef DAS < handle
    properties(Access=public)
        CycleIndex;
        Assimilation_cycle_tot;
        Assimilation_cycle;
        restart_flag;
        restart_cycle;
        nobs;
        t_init;
        t_offset;
        t_forecast;
        tobs_assim;
        tmax_assim;
        tmax;
        t_assim_deb;
        t_assim_fin;
        itobs_valve;
        itobs_sum;
        itobs_number;
        ittru_sum;
        itobs_deb;
        itobs_fin;
        ittru;
        ittru_assim_deb;
        ittru_assim_fin;
        ittru_number;
        itmax;
        itinit;
        Assimilation_flag;
        VarRangeDeb;
        VarRangeFin;
        augState;
        Assimflag_suffix;
        ParameterEstimation_flag;
        BackgroundState_flag;
        Algo_ControlVector_Type;
        Algo_Precondition_Type;
        Algo_Localization_Type;
        Algo_EnsembleUpdate_Type;
        sb;
        sf;
        sa;
        Xb;
        Xf;
        Xa;
        RMSXobsXt;
        RMSXbXt;
        RMSXaXt;
        RMSXbXt_forecast;
        RMSXaXt_forecast;
        Write_Res_Each_Cycle;
    end
    
    methods
        function self=DAS(par)
            self.CycleIndex=par.CycleIndex;
            self.Assimilation_cycle_tot=par.Assimilation_cycle_tot;
            self.Assimilation_cycle=par.Assimilation_cycle;
            self.nobs=par.nobs;
            self.restart_flag=par.restart_flag;
            self.restart_cycle=par.restart_cycle;
                    
            self.t_init=par.t_init;
            self.t_offset=par.t_offset;
            self.t_forecast=par.t_forecast;
            self.tobs_assim=par.tobs_assim;
            
            self.tmax_assim=par.tmax_assim;
            self.tmax=par.tmax;

            self.t_assim_deb=par.t_assim_deb;
            self.t_assim_fin=par.t_assim_fin;

            self.itobs_sum=par.itobs_sum;

            self.ittru_sum=par.ittru_sum;

            self.itobs_number=par.itobs_number;
            self.itobs_deb=par.itobs_deb;
            self.itobs_fin=par.itobs_fin;

            self.ittru=par.ittru;
            self.ittru_assim_deb=par.ittru_assim_deb;
            self.ittru_number=par.ittru_number;
            self.ittru_assim_fin=par.ittru_assim_fin;

            self.itmax=par.itmax;
            self.itinit=par.itinit;

            self.Assimilation_flag=par.Assimilation_flag;

            self.VarRangeDeb=par.VarRangeDeb;
            self.VarRangeFin=par.VarRangeFin;
            self.Assimflag_suffix=par.Assimflag_suffix;
               
            self.ParameterEstimation_flag=par.ParameterEstimation_flag;
            self.augState=par.augState;

            self.BackgroundState_flag=par.BackgroundState_flag;
            
            self.Algo_ControlVector_Type=par.Algo_ControlVector_Type;
            self.Algo_Precondition_Type=par.Algo_Precondition_Type;
            self.Algo_Localization_Type=par.Algo_Localization_Type;
            self.Algo_EnsembleUpdate_Type=par.Algo_EnsembleUpdate_Type;
            
            self.sb=par.sb;
            
            self.Write_Res_Each_Cycle=par.Write_Res_Each_Cycle;
        end
        
        function set.CycleIndex(self,CycleInd)
            self.CycleIndex=CycleInd;
        end
        
        function set.Xb(self,X_background)
            self.Xb=X_background;
        end
        
        function set.sa(self,s_analysis)
            self.sa=s_analysis;
        end
        
        function self=BackgroundState(self,dym_obj,res_obj)

            if strcmp(self.restart_flag,'Off')
                disp('make the background trajectory');  
                self.Xb=modelrun(self.sb(:,:,1:3),dym_obj,self,'state'); 
            else
                disp('load the restart background trajectory');
                self.Xb=res_obj(self.CycleIndex-1).Xb;
            end
        end
        
        function self=InitialState(self,dym_obj,res_obj)
            disp('make the initial state')  
            if self.CycleIndex==1
                if self.t_offset==0
                    self.sf=self.sb;
                else 
                    self.sf(:,:,1:3)=self.Xb(:,:,:,self.t_offset/dym_obj.dt+1);
                end
            else
               disp('load the restart initial state');
               self.sf(:,:,1:3)=res_obj(self.CycleIndex-1).Xa(:,:,:,end);
            end
        end
        
        function self=ForecastState(self,dym_obj)            
            disp('make the forecast state trajectory')  
            self.Xf=modelrun(self.sf(:,:,1:3),dym_obj,self,'forecast'); 
        end
        
        
        function self=AnalysisState(self,dym_obj)
            if 1%self.CycleIndex~=self.Assimilation_cycle
                disp('make the analysis trajectory'); 
                self.Xa=modelrun(self.sa(:,:,1:3),dym_obj,self,'analysis'); 
            else
                h=interp(self.sa(1,:,1),4); h=h(1:dym_obj.nyobs);                
                u=interp(self.sa(1,:,2),4); u=u(1:dym_obj.nyobs);
                v=interp(self.sa(1,:,3),4); v=v(1:dym_obj.nyobs);
                xa=zeros(dym_obj.nxobs,dym_obj.nyobs,3);
                xa(:,:,1)=[h;h];
                xa(:,:,2)=[u;u];
                xa(:,:,3)=[v;v];
                
                disp('make the analysis trajectory'); 
                self.Xa=modelrun(xa,dym_obj,self,'analysis_alone'); 
            end
        end
        
        function self=CalculateRMS(self,dym_obj)
            Xtrue=dym_obj.Xt;
            Uobs=dym_obj.Xobs(:,:,self.VarRangeDeb:self.VarRangeFin,self.itobs_deb(self.CycleIndex):self.itobs_fin(self.CycleIndex));

%             rmsXbXt=sqrt(mean(mean((self.Xb(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))).^2)));
%             self.RMSXbXt=squeeze(rmsXbXt)';

            Uobs=squeeze(Uobs);     
            
            rmsXobsXt=sqrt(mean(mean((Uobs-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,self.t_assim_deb(self.CycleIndex)/dym_obj.dt+self.ittru)).^2)));
            self.RMSXobsXt=squeeze(rmsXobsXt)';

            rmsXbXt=sqrt(mean(mean((self.Xb(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))).^2)));
            self.RMSXbXt=squeeze(rmsXbXt)';

            if self.CycleIndex~=self.Assimilation_cycle
            rmsXaXt=sqrt(mean(mean((self.Xa(:,:,self.VarRangeDeb:self.VarRangeFin,:)-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))).^2)));
            else
            rmsXaXt=sqrt(mean(mean((self.Xa(:,:,self.VarRangeDeb:self.VarRangeFin,:)-dym_obj.Xt(:,:,self.VarRangeDeb:self.VarRangeFin,self.ittru_assim_deb(self.CycleIndex):self.ittru_assim_fin(self.CycleIndex))).^2)));
            end
            
            self.RMSXaXt=squeeze(rmsXaXt)';
        end
        
        function self=ForecastAndPlot(self,dym_obj,rescycle,ResultDir)
            self=self.ForecastAlone(dym_obj,rescycle);
            self=self.PlotRMSAll(dym_obj,rescycle,ResultDir);
        end
        
        function self=ForecastAlone(self,dym_obj,rescycle)
            
            Xtrue=dym_obj.Xt;
            
            if self.t_forecast~=0            
                x0=rescycle(self.Assimilation_cycle).Xa(:,:,:,end);
                
                x0_alone=x0;

                disp('make the forecast alone state trajectory')  
                Xf_alone=modelrun(x0_alone,dym_obj,self,'forecast_alone'); 

                rmsXbXt=sqrt(mean(mean((self.Xb(:,:,self.VarRangeDeb:self.VarRangeFin,floor((self.t_offset+self.tobs_assim*self.Assimilation_cycle)/dym_obj.dt)+2:end)-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,floor((self.t_offset+self.tobs_assim*self.Assimilation_cycle)/dym_obj.dt)+2:end)).^2)));
                self.RMSXbXt_forecast=squeeze(rmsXbXt)';

                rmsXaXt=sqrt(mean(mean((Xf_alone(:,:,self.VarRangeDeb:self.VarRangeFin,2:end)-Xtrue(:,:,self.VarRangeDeb:self.VarRangeFin,floor((self.t_offset+self.tobs_assim*self.Assimilation_cycle)/dym_obj.dt)+2:end)).^2)));
                self.RMSXaXt_forecast=squeeze(rmsXaXt)';
            end
        end
        
        function self=PlotRMS(self,dym_obj,ens_obj,opt_obj,loc_obj,ResultDir)
            to=self.t_assim_deb(self.CycleIndex):dym_obj.dtobs:self.t_assim_fin(self.CycleIndex)-dym_obj.dtobs;
            t =self.t_assim_deb(self.CycleIndex):dym_obj.dt:self.t_assim_fin(self.CycleIndex);

            f=figure;
            set(f,'name','RMS of Analysis, Background and Observation w.r.t True solution','numbertitle','off');

            nvard=dym_obj.nstate-1;
            
            subplot(nvard,1,1);
            semilogy(t,self.RMSXbXt(:,1),'-g')
            hold on
            semilogy(to,self.RMSXobsXt(:,1),'bo')
            if strcmp(self.Assimilation_flag,'All')||strcmp(self.Assimilation_flag,'Height_only');
            semilogy(t,self.RMSXaXt(:,1),'-k')
            end
            hold off
            axis tight
            xlabel('t');ylabel('RMS(h)');

            subplot(nvard,1,2);
            semilogy(t,self.RMSXbXt(:,2),'-g')
            hold on
            semilogy(to,self.RMSXobsXt(:,2),'bo')
            if strcmp(self.Assimilation_flag,'All')||strcmp(self.Assimilation_flag,'Velocity_only');
            semilogy(t,self.RMSXaXt(:,2),'-k')
            end
            hold off
            axis tight
            xlabel('t');ylabel('RMS(u)');
            
            switch self.Algo_Localization_Type
            case 'None'
                saveas(f,[ResultDir '/obs',self.Assimflag_suffix,'_it',num2str(opt_obj.MaxIter_Outer),'_m',num2str(self.nobs),...
                '_N',num2str(ens_obj.nens),ens_obj.EnType_suffix,'_NoLocal','_toffset',...
                num2str(self.t_offset),'_Cycle',num2str(self.CycleIndex),'.fig'],'fig');
            case 'Localize_Covariance'
                saveas(f,[ResultDir '/obs',self.Assimflag_suffix,'_it',num2str(opt_obj.MaxIter_Outer),'_m',num2str(self.nobs),...
                '_N',num2str(ens_obj.nens),ens_obj.EnType_suffix,'_cod',num2str(loc_obj.cod),'_r',num2str(loc_obj.r),...
                '_toffset',num2str(self.t_offset),'_Cycle',num2str(self.CycleIndex),'.fig'],'fig');        
            case 'Local_Analysis'
                 saveas(f,[ResultDir '/obs',self.Assimflag_suffix,'_it',num2str(opt_obj.MaxIter_Outer),'_m',num2str(self.nobs),...
                '_N',num2str(ens_obj.nens),ens_obj.EnType_suffix,'_locx',num2str(loc_obj.LocalSizex),'_locy',num2str(loc_obj.LocalSizey),...
                '_toffset',num2str(self.t_offset),'_Cycle',num2str(self.CycleIndex),'.fig'],'fig');        
            end
        end
        
        function self=SaveResult(self,dym_obj,ens_obj,rescycle_obj,RestartDir)
            Uobs=dym_obj.Xobs(:,:,:,self.itobs_deb(self.CycleIndex):self.itobs_fin(self.CycleIndex));
            Uobs=squeeze(Uobs);
                        
            rescycle_obj(self.CycleIndex)=rescycle_obj(self.CycleIndex).ResultCycle(self.CycleIndex,Uobs,self.Xb,self.Xf,self.Xa,self.RMSXbXt,self.RMSXaXt,self.RMSXobsXt,ens_obj.xensf);

            res=rescycle_obj(self.CycleIndex);
            if strcmp(self.Write_Res_Each_Cycle,'On')
                save(strcat(RestartDir,'/rst_cycle',num2str(self.CycleIndex),'.mat'),'res');
            end
        end
        
        function self=ResetforNextCycle(self)
            self.sf=[];
            self.sa=[];
            self.Xf=[];
            self.Xa=[];
            self.RMSXobsXt=[];
            self.RMSXbXt=[];
            self.RMSXaXt=[];
        end
        
        function self=PlotRMSAll(self,dym_obj,rescycle,ResultDir)
            nvard=dym_obj.nstate;
            
            RMSXbXtAll=zeros(floor((self.t_forecast+self.Assimilation_cycle*self.tobs_assim)/dym_obj.dt)+1,dym_obj.nstate);
            RMSXaXtAll=zeros(floor((self.t_forecast+self.Assimilation_cycle*self.tobs_assim)/dym_obj.dt)+1,dym_obj.nstate);
            RMSXobsXtAll=zeros(self.itobs_sum,dym_obj.nstate);
            
            for i=1:self.Assimilation_cycle-1
            RMSXbXtAll(round(self.tobs_assim/dym_obj.dt*(i-1)+1):round(self.tobs_assim/dym_obj.dt*i),:)=rescycle(i).RMSXbXt(1:self.ittru_number(i)-1,:);
            RMSXaXtAll(round(self.tobs_assim/dym_obj.dt*(i-1)+1):round(self.tobs_assim/dym_obj.dt*i),:)=rescycle(i).RMSXaXt(1:self.ittru_number(i)-1,:);
            RMSXobsXtAll(self.itobs_deb(i):self.itobs_fin(i),:)=rescycle(i).RMSXobsXt;
            end
            RMSXbXtAll(round((self.Assimilation_cycle-1)*self.tobs_assim/dym_obj.dt+1):round(self.Assimilation_cycle*self.tobs_assim/dym_obj.dt+1),:)=rescycle(self.Assimilation_cycle).RMSXbXt;
            RMSXaXtAll(round((self.Assimilation_cycle-1)*self.tobs_assim/dym_obj.dt+1):round(self.Assimilation_cycle*self.tobs_assim/dym_obj.dt+1),:)=rescycle(self.Assimilation_cycle).RMSXaXt;
            RMSXobsXtAll(self.itobs_deb(self.Assimilation_cycle):self.itobs_deb(self.Assimilation_cycle)+self.nobs-1,:)=rescycle(self.Assimilation_cycle).RMSXobsXt;
            
            if self.t_forecast~=0
            RMSXbXtAll(round(self.Assimilation_cycle*self.tobs_assim/dym_obj.dt+2):end,:)=self.RMSXbXt_forecast;
            RMSXaXtAll(round(self.Assimilation_cycle*self.tobs_assim/dym_obj.dt+2):end,:)=self.RMSXaXt_forecast;
            end
                        
            save(strcat(ResultDir,'/RMSXobsXtAll.mat'),'RMSXobsXtAll');
            save(strcat(ResultDir,'/RMSXbXtAll.mat'),'RMSXbXtAll');
            save(strcat(ResultDir,'/RMSXaXtAll.mat'),'RMSXaXtAll');
            
            to=self.t_offset:dym_obj.dtobs:self.tmax_assim-dym_obj.dtobs;
            t1=self.t_offset:dym_obj.dt:self.tmax;

            f=figure;
            set(f,'name','RMS of Analysis, Background and Obervation w.r.t True solution','numbertitle','off');
            subplot(nvard,1,1);
            semilogy(t1,RMSXbXtAll(:,1),'-g')
            hold on
            if strcmp(self.Assimilation_flag,'All')||strcmp(self.Assimilation_flag,'Height_only');
            semilogy(to,RMSXobsXtAll(:,1),'bo')
            end
            semilogy(t1,RMSXaXtAll(:,1),'-k')
            hold off
            xlabel('t');ylabel('RMS(h)');

            axis tight

            subplot(nvard,1,2);
            semilogy(t1,RMSXbXtAll(:,2),'-g')
            hold on
            if strcmp(self.Assimilation_flag,'All')||strcmp(self.Assimilation_flag,'Velocity_only');
            semilogy(to,RMSXobsXtAll(:,2),'bo')
            end
            semilogy(t1,RMSXaXtAll(:,2),'-k')
            hold off
            xlabel('t');ylabel('RMS(u)');
            
            axis tight
                        
            subplot(nvard,1,3);
            plot(t1,RMSXbXtAll(:,3),'-g')
            hold on
            if strcmp(self.Assimilation_flag,'All')||strcmp(self.Assimilation_flag,'Velocity_only');
            plot(to,RMSXobsXtAll(:,3),'bo')
            end
            plot(t1,RMSXaXtAll(:,3),'-k')
            hold off
            xlabel('t');ylabel('RMS(v)');
            
            axis tight
            
            saveas(f,strcat(ResultDir,'/obsAll_',self.Assimflag_suffix,'_m',num2str(self.nobs),'_toffset',num2str(self.t_offset),'.fig'),'fig');
        end
    
    end
end