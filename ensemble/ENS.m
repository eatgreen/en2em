   % ======================================================================
   %                       ***  Class ENS  ***
   % 4DENVar : Generic ensemble class 
   %======================================================================

   %----------------------------------------------------------------------
   %    ENS                  : Constructor 
   %    EnsembleInitial      : Prescribe result
   %    EnsembleForecast     : Ensemble state evolution from t0 -> tf
   %    EnsembleAnomaly      : Compute ensemble anomaly matrix
   %    EnsembleAnomalyLocal : Compute ensemble anomaly matrix, local analysis 
   %    EnsembleInnovation   : Compute ensemble innovation
   %    EnsembleIntegration  : Ensemble state evolution to next cycle
   %----------------------------------------------------------------------
   
classdef ENS < handle
    properties(Access=public)
        nens;
        codENS;
        EnsembleType_flag;
        EnType_suffix;
        EnsembleInflation_flag;
        InflationFactor;
        Inf_suffix;
        xensf;
        xensa;
        Xens;
        Sens;
        SensR;
        HMSbR;
        VInnoRENS;
    end
    
    
    methods
        function self=ENS(par)
            self.nens=par.nens;
            self.codENS=par.codENS;
            
            self.EnsembleType_flag=par.EnsembleType_flag;
            self.EnType_suffix=par.EnType_suffix;
            self.EnsembleInflation_flag=par.EnsembleInflation_flag;
            self.InflationFactor=par.InflationFactor;
            self.Inf_suffix=par.Inf_suffix;
        end

        function set.codENS(self,codE)
            self.codENS=codE;
        end
        
        function set.xensf(self,xens)
            self.xensf=xens;
        end
        
        function set.xensa(self,xens)
            self.xensa=xens;
        end
        
        function set.Xens(self,Xens)
            self.Xens=Xens;
        end
        
        function set.Sens(self,Sens)
            self.Sens=Sens;
        end
        
        function self=EnsembleInitial(self,dym_obj,das_obj)
            xinit=das_obj.Xb(:,:,:,1);
            
            xens0=zeros(dym_obj.nx,dym_obj.ny,3,self.nens);
            self.xensf=zeros(dym_obj.nx,dym_obj.ny,3,self.nens);

            xref=dym_obj.Xt(:,:,:,1);
            Err_hb=sqrt(mean(mean((xinit(:,:,1)-xref(:,:,1)).^2)));   

            switch self.EnsembleType_flag
                case 'Gaussian'
                %Initialize the ensemble fields, add perturbation

                    fullnoise=pseudo2dM( dym_obj.nx, dym_obj.ny, self.nens, self.codENS, self.codENS, 1, 1, 0 );
                        
%                         fullnoise=zeros(dym_obj.nx, dym_obj.ny, self.nens);
%                         ncid=netcdf.open('/Users/yang/Documents/4DEnVarCpp/data/ensemble_noise1x1.nc','NC_NOWRITE');
% 
%                         for i=1:self.nens
%                             varname=strcat('h',num2str(i-1));
%                             varid=netcdf.inqVarID(ncid,varname);
%                             fullnoise(:,:,i)=netcdf.getVar(ncid,varid);
%                         end
%                         netcdf.close(ncid);
                        
                    dxens0=dym_obj.Err_beta*dym_obj.Err_hb*fullnoise;

                    for i=1:self.nens
                        xens0(:,:,1,i)  =xinit(:,:,1)+dxens0(:,:,i);
                        xens0(:,:,2:3,i)=xinit(:,:,2:3);%+dxens0(:,:,2:3,i);
                    end                                                                           
            end
            
            %Ensemble fields need to be updated to the beginning of assimilation
            %window when t_init~=t_assim_deb
    
            if das_obj.t_offset==0
                self.xensf=xens0;
            else
                
                disp('make the ensemble initial state')  

                parfor i=1:self.nens

                    uensf(:,:,:,i)=modelrun(xens0(:,:,:,i),dym_obj,das_obj,'ensembleinitial'); 
            
                end

                self.xensf=uensf;
                clear uensf xens0  
            end
        end
        
        function self=EnsembleForecast(self,dym_obj,das_obj,res_obj)
                        
            if strcmp(das_obj.restart_flag,'On')
            self.xensf=res_obj(das_obj.CycleIndex-1).xensf;
            end
            
            uensf=self.xensf;
                        
            disp('make the ensemble forecast state trajectory')  

            parfor i=1:self.nens

                X=uensf(:,:,:,i);

                Xti=modelrun(X,dym_obj,das_obj,'ensembleforecast'); 

                Uens(:,:,:,i,:)=Xti;
            end
                        
            self.Xens=Uens;
            clear Uens
            
            self.Sens=zeros(dym_obj.nx,dym_obj.ny,das_obj.augState,self.nens,das_obj.itobs_number);
            self.Sens(:,:,1:3,:,:)=self.Xens;
        end
            
        function self=EnsembleAnomaly(self,dym_obj,das_obj)
            
            MeanSens=mean(self.Sens(:,:,:,:,:),4);%Ensemble mean
 
            HSens=self.Sens;
            HMeanSens=MeanSens;
            clear Sens MeanSens

            for k=1:das_obj.augState
                for j=1:das_obj.itobs_number
                    for i=1:self.nens
                    HMSb(:,:,k,i,j)=1/sqrt(self.nens-1)*(HSens(:,:,k,i,j)-HMeanSens(:,:,k,j));
                    end
                end
            end

            self.HMSbR=reshape(HMSb,[dym_obj.nx*dym_obj.ny*das_obj.augState, self.nens, das_obj.itobs_number]);
        end

        function HMSbRmn=EnsembleAnomalyLocal(self,LocalIndex,lx,ly,dym_obj,das_obj,loc_obj)
            
            MeanSens=mean(self.Sens(:,:,:,:,:),4);%Ensemble mean
 
            Sensmn=loc_obj.LocalAnalysisOperator(self.Sens,LocalIndex,dym_obj);
            MeanSensmn=loc_obj.LocalAnalysisOperator(MeanSens,LocalIndex,dym_obj);
            
            for k=1:das_obj.augState
              for j=1:das_obj.itobs_number
                for i=1:self.nens
                HMSbmn(:,:,k,i,j)=1/sqrt(self.nens-1)*(Sensmn(:,:,k,i,j)-MeanSensmn(:,:,k,j));
                end
              end
            end

            HMSbRmn=reshape(HMSbmn,[lx*ly*das_obj.augState, self.nens, das_obj.itobs_number]);
            clear HMSbmn
        end

        
        function self=EnsembleInnovation(self,dym_obj,das_obj)
                
            HXf=das_obj.Xf(:,:,:,das_obj.ittru);
            HXf=squeeze(HXf);
            
            Uens=zeros(dym_obj.nx,dym_obj.ny,3,self.nens+1,das_obj.itobs_number);
            Uens(:,:,:,1:self.nens,:)=self.Xens;
            Uens(:,:,:,self.nens+1,:)=HXf;

            XobsENS=dym_obj.XobsENS(:,:,:,:,das_obj.itobs_deb(das_obj.CycleIndex):das_obj.itobs_fin(das_obj.CycleIndex));               
           
           %Calculate the innovation vector 

            VInnoENS=squeeze(XobsENS(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,:,:))-squeeze(Uens(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,:,:));

            self.VInnoRENS=reshape(VInnoENS, [], self.nens+1, das_obj.itobs_number);
        end
        
        function self=EnsembleIntegration(self,dym_obj,das_obj)
            if das_obj.CycleIndex==das_obj.Assimilation_cycle_tot
                return;    
            else
                disp('make ensemble forecast for next cycle based on ensemble analysis of current cycle');

                uensa=self.xensa;

                for i=1:self.nens
                    uensf(:,:,:,i)=modelrun(uensa(:,:,:,i),dym_obj,das_obj,'ensembleintegration'); 
                end
                self.xensf=uensf;
            end
        end
        
        function self=EnsembleInflation(self,dym_obj,das_obj)
            disp('Inflate the analysis ensemble');
            xensa_inf=zeros(dym_obj.nx,dym_obj.ny,3,self.nens); 

            Meanxensa=mean(self.xensa(:,:,:,:),4);%Ensemble mean

            for i=1:self.nens
                xensa_inf(:,:,:,i)=self.InflationFactor*self.xensa(:,:,:,i)+(1-self.InflationFactor)*Meanxensa;
            end
            
            self.xensa=xensa_inf;
        end
    end
end