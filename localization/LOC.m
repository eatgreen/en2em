   % ======================================================================
   %                       ***  Class LOC  ***
   % 4DENVar : Generic localization class 
   %======================================================================

   %----------------------------------------------------------------------
   %    LOC                    : Constructor 
   %    LocalizationConstruct  : Compute global correlation matrix  
   %    LocalizationCovariance : Compute localized error covariance matrix
   %    LocalAnalysisOperator  : Map global state to local analysis
   %----------------------------------------------------------------------
  
classdef LOC < handle
    properties(Access=public)
        Localization_Type;
        cod;                %Cutoff distance
        r;                  %Truncated number of eigselfvalues
        sqrtC;              %sqrt of correlation matrix C
        HMPbR;              %sqrt of localized B
        LocalSizex;         %local size x
        LocalSizey;         %local size y
        R_factor;
        R_factor_effective;
    end
       
    methods 
    	function self=LOC(par,das_obj,dym_obj)
    		self.Localization_Type=par.Algo_Localization_Type;
            self.cod=par.cod;
    		if strcmp(self.Localization_Type,'Localize_Covariance')
	    		self.r=par.r;
	    		self=self.LocalizationConstruct(das_obj,dym_obj);
            elseif strcmp(self.Localization_Type,'Local_Analysis')
	    		self.LocalSizex=par.LocalSizex;
	    		self.LocalSizey=par.LocalSizey;
                self=self.LocalizationObservationFactor(dym_obj);
	    	end
    	end

    	function self=LocalizationConstruct(self,das_obj,dym_obj)
                                
            c=zeros(dym_obj.nx,dym_obj.ny,dym_obj.nx,dym_obj.ny);
            % Gaspari 1999
            for i=1:dym_obj.nx
                for j=1:dym_obj.ny    
                    for k=max(1,i-self.cod):min(i+self.cod,dym_obj.nx)
                        for l=max(1,j-self.cod):min(j+self.cod,dym_obj.ny)
                            s=self.cod/2*dym_obj.dx;
                            z=sqrt((dym_obj.xx(i,j)-dym_obj.xx(k,l))^2+(dym_obj.yy(i,j)-dym_obj.yy(k,l))^2);
                            if z<=s
                                c(i,j,k,l)=-(z/s)^5/4+(z/s)^4/2+5*(z/s)^3/8-5*(z/s)^2/3+1;
                            elseif z>s && z<=2*s
                                c(i,j,k,l)=(z/s)^5/12-(z/s)^4/2+5*(z/s)^3/8+5*(z/s)^2/3-5*(z/s)+4-2/(3*z/s);
            %                else
            %                    c(i,j,k,l)=0;
                            end
                        end
                    end
                end
            end
            cc=reshape(c,[dym_obj.nx*dym_obj.ny,dym_obj.nx*dym_obj.ny]);
            clear c
            
			C=repmat(cc,das_obj.augState,das_obj.augState);
			clear cc

			S=sparse(C);
			clear C
			[E,lamba]=eigs(S,self.r);
			clear S
			lamba=diag(lamba);

			%Truncate E and lambda
			sqrC= E;
			clear E
			lamba_r= lamba(1:self.r);
			clear lamba
			lamba_r_sqrt=diag(sign(lamba_r(:)).*sqrt(abs(lamba_r(:))));
			clear lamba_r

			self.sqrtC=sqrC*lamba_r_sqrt;
        end

        function self=LocalizationObservationFactor(self,dym_obj)

            [lxx,lyy]=meshgrid(1:max(dym_obj.nx,dym_obj.ny),1:max(dym_obj.nx,dym_obj.ny));

            self.R_factor=zeros(2*self.LocalSizex+1,2*self.LocalSizey+1);

            for i=1:min(dym_obj.nx,2*self.LocalSizex+1)
                for j=1:min(dym_obj.ny,2*self.LocalSizey+1)
                    dist=sqrt((lxx(i,j)-lxx(self.LocalSizex+1,self.LocalSizey+1))^2+(lyy(i,j)-lyy(self.LocalSizex+1,self.LocalSizey+1))^2);
                    self.R_factor(i,j)=exp(-dist^2/(2*(self.cod/3.65)^2));
                end
            end
        end
        
        function R_factor_effective=LocalizationObservationFactorEffective(self,lx,ly,reliIndex,reljIndex)

            R_factor_effective=self.R_factor(self.LocalSizex+2-reliIndex:self.LocalSizex+1-reliIndex+lx,self.LocalSizey+2-reljIndex:self.LocalSizey+1-reljIndex+ly);
            assert(size(R_factor_effective,1)==lx);   
            assert(size(R_factor_effective,2)==ly);      
        end
        
    	function self=LocalizationCovariance(self,das_obj,dym_obj,ens_obj)
                            
            HMPb=zeros(dym_obj.nx*dym_obj.ny*das_obj.augState,self.r,ens_obj.nens,das_obj.itobs_number);

            disp('make HMPb');

            for i=1:ens_obj.nens
                for j=1:self.r
                    for k=1:das_obj.itobs_number
                        HMPb(:,j,i,k)=self.sqrtC(:,j).*ens_obj.HMSbR(:,i,k);
                    end
                end
            end

            disp('make HMPbR');

            self.HMPbR=reshape(HMPb,[dym_obj.nx*dym_obj.ny*das_obj.augState,self.r*ens_obj.nens,das_obj.itobs_number]);
        end

        function Xmn=LocalAnalysisOperator(self,X,localindex,dym_obj)
                        
            iInd=mod(localindex,dym_obj.nx);
            if iInd==0
              iInd=dym_obj.nx;
            end
            jInd=(localindex-iInd)/dym_obj.nx+1;
            k=size(size(X));
            switch k(2)
            case 3
               Xmn=X(max(iInd-self.LocalSizex,1):min(iInd+self.LocalSizex,dym_obj.nx),max(jInd-self.LocalSizey,1):min(jInd+self.LocalSizey,dym_obj.ny),:);
            case 4
               Xmn=X(max(iInd-self.LocalSizex,1):min(iInd+self.LocalSizey,dym_obj.nx),max(jInd-self.LocalSizey,1):min(jInd+self.LocalSizey,dym_obj.ny),:,:);
            case 5
               Xmn=X(max(iInd-self.LocalSizex,1):min(iInd+self.LocalSizey,dym_obj.nx),max(jInd-self.LocalSizey,1):min(jInd+self.LocalSizey,dym_obj.ny),:,:,:);   
            otherwise   
               error('X must be 3D, 4D or 5D')
            end
        end

	end
end