 % Y=NF2D(X,option,dt,dx)
% Generates the numerical flux F = (dt/dx)*(F_(i-1/2)-F_(i+1/2))
% Input:
%   - X: state
%   - option: contains the step, Boundary conditions and linearized state
%   - dt: time step
%   - dx: space step
% Output:
%   - Y: x-axis flux
%
% Author: C. Robinson,

function Flux=Advection(X,option,dx,dy)

% if (strcmp(option.step,'adj')==0)
%     X=foo1(X,option.step,option.X0);
% end
% X=foo1(X,option.dynstep,option.X0);

switch option.dynstep    
    case 'dyn'        
        % Boundary conditions -----------------
        XBC = BC2D(foo1(X),option);
        hs = XBC.XS(:,:,1); us = XBC.XS(:,:,2); vs = XBC.XS(:,:,3);
        hn = XBC.XN(:,:,1); un = XBC.XN(:,:,2); vn = XBC.XN(:,:,3);
        he = XBC.XE(:,:,1); ue = XBC.XE(:,:,2); ve = XBC.XE(:,:,3);
        hw = XBC.XW(:,:,1); uw = XBC.XW(:,:,2); vw = XBC.XW(:,:,3);
                
        % Horizontal flux ---------------------
        F = RoeFlux_2D(hw,he,uw,ue,vw,ve,1);        
        % Vertical flux -----------------------
        G = RoeFlux_2D(hn,hs,un,us,vn,vs,0);
        
        Flux= -( (F(1:(end-1),2:end,:)-F(1:(end-1),1:(end-1),:))/dx ...
          +  (G(2:end,1:(end-1),:)-G(1:(end-1),1:(end-1),:))/dy ) ;
        
    case 'tan'
        % Boundary conditions -----------------
        X=foo1(X,'tan',option.X0);
        [XBC,X0BC] = BC2D(X,option); % NB! BC2D uses foo1(option.X0)
        he = XBC.XE(:,:,1); ue = XBC.XE(:,:,2); ve = XBC.XE(:,:,3);
        hw = XBC.XW(:,:,1); uw = XBC.XW(:,:,2); vw = XBC.XW(:,:,3);
        hn = XBC.XN(:,:,1); un = XBC.XN(:,:,2); vn = XBC.XN(:,:,3);
        hs = XBC.XS(:,:,1); us = XBC.XS(:,:,2); vs = XBC.XS(:,:,3);
        h0e = X0BC.XE(:,:,1); u0e = X0BC.XE(:,:,2); v0e = X0BC.XE(:,:,3);
        h0w = X0BC.XW(:,:,1); u0w = X0BC.XW(:,:,2); v0w = X0BC.XW(:,:,3);
        h0n = X0BC.XN(:,:,1); u0n = X0BC.XN(:,:,2); v0n = X0BC.XN(:,:,3);
        h0s = X0BC.XS(:,:,1); u0s = X0BC.XS(:,:,2); v0s = X0BC.XS(:,:,3);
        
        % Horizontal flux ---------------------
        F = RoeFluxTangent(hw,he,uw,ue,vw,ve,h0w,h0e,u0w,u0e,v0w,v0e,1);        
        % Vertical flux -----------------------
        G = RoeFluxTangent(hn,hs,un,us,vn,vs,h0n,h0s,u0n,u0s,v0n,v0s,0);
        
        Flux= -( (F(1:(end-1),2:end,:)-F(1:end-1,1:(end-1),:))/dx  ...
            + (G(2:end,1:(end-1),:)-G(1:(end-1),1:(end-1),:))/dy ) ;

    case 'adj'
        
        option.step='dyn';
        X0BC  = BC2D(foo1(option.X0),option);
        h0s = X0BC.XS(:,:,1); u0s = X0BC.XS(:,:,2); v0s = X0BC.XS(:,:,3);
        h0n = X0BC.XN(:,:,1); u0n = X0BC.XN(:,:,2); v0n = X0BC.XN(:,:,3);
        h0e = X0BC.XE(:,:,1); u0e = X0BC.XE(:,:,2); v0e = X0BC.XE(:,:,3);
        h0w = X0BC.XW(:,:,1); u0w = X0BC.XW(:,:,2); v0w = X0BC.XW(:,:,3);
        
        option.step='adj';
        nx=size(X,1);ny=size(X,2);        
        Fb=zeros(nx+1,ny+1,3);
        Fb(1:(end-1),2:end,:) = -X./dx;
        Fb(1:end-1,1:(end-1),:) = Fb(1:end-1,1:(end-1),:) + X./dx;        
        Gb=zeros(nx+1,ny+1,3);
        Gb(2:end,1:(end-1),:) = -X./dy;
        Gb(1:end-1,1:(end-1),:) = Gb(1:end-1,1:(end-1),:) + X./dy;
                
        [hnb,hsb,unb,usb,vnb,vsb]= RoeFluxAdjoint(Gb,h0n,h0s,u0n,u0s,v0n,v0s,0);       
        Xb.XS=zeros(nx+1,ny+1,3);
        Xb.XS(:,:,3) = vsb; Xb.XS(:,:,2) = usb; Xb.XS(:,:,1) = hsb;
        Xb.XN=zeros(nx+1,ny+1,3);
        Xb.XN(:,:,3) = vnb; Xb.XN(:,:,2) = unb; Xb.XN(:,:,1) = hnb;     
        
        [hwb,heb,uwb,ueb,vwb,veb]= RoeFluxAdjoint(Fb,h0w,h0e,u0w,u0e,v0w,v0e,1);        
        Xb.XE=zeros(nx+1,ny+1,3);
        Xb.XE(:,:,3) = veb; Xb.XE(:,:,2) = ueb; Xb.XE(:,:,1) = heb;
        Xb.XW=zeros(nx+1,ny+1,3);
        Xb.XW(:,:,3) = vwb; Xb.XW(:,:,2) = uwb; Xb.XW(:,:,1) = hwb;     
                
        XBC = BC2D(Xb,option); 
        Flux=foo1(XBC.Xb,'adj',option.X0);%foo1(option.X0));       
end

end
