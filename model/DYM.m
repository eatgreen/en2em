   % ======================================================================
   %                       ***  Class DYM  ***
   % 4DENVar : Generic dynamic and observation class
   %======================================================================

   %----------------------------------------------------------------------
   %    DYM               : Constructor
   %----------------------------------------------------------------------

classdef DYM
	properties(Access=public)
        dim;
		g;
		dynstep;
		fluxType;
		BC;
		Lx;
		Ly;
		nx;
		ny;
		dx;
		dy;
		x;
		y;
		xx;
		yy;
        nstate;
        nsize;
		nxobsdata;
		nyobsdata;
		xobs;
		yobs;
		xxobs;
		yyobs;
		nxobs;
		nyobs;
		dxobs;
		dyobs;
		gridratio;
		dt;
		dtdata;
		dtobs;
		t_ratio;
		t_ratiodata;
		orderT;
		Slope;
        Err_Type;
		Err_hb;
		Err_ub;
		Err_vb;
		Err_ho;
		Err_uo;
		Err_vo;
        Err_beta;
        Inc_beta;
        Inc_betaENS;
	end

	methods
		function self=DYM(par)           
            self.g=par.g;
            self.dim=par.dim;
            self.dynstep=par.dynstep;
            self.BC = par.BC;
            self.fluxType=par.BC; 
            self.Lx=par.Lx;
            self.Ly=par.Ly;
            
            %Physical space resolution-----
            self.dx=par.dx; 
            self.dy=par.dy;
            self.x =par.x;
            self.y =par.y;
            self.xx=par.xx;
            self.yy=par.yy;
            self.nx=par.nx;
            self.ny=par.ny;

            %State space size
            self.nstate=par.nstate;
            self.nsize =par.nsize;

             %Observation space resolution----- 
            self.dxobs=par.dxobs; 
            self.dyobs=par.dyobs;
            self.xobs =par.xobs;
            self.yobs =par.yobs;
            self.xxobs=par.xxobs;
            self.yyobs=par.yyobs;
            self.nxobs=par.nxobs;
            self.nyobs=par.nyobs;

            %Temporal space resolution----
            self.dt         =par.dt;
            self.dtdata     =par.dtdata;
            self.dtobs      =par.dtobs;
            self.t_ratio    =par.t_ratio; 
            self.t_ratiodata=par.t_ratiodata; 
            self.orderT     =par.orderT;
            
            self.Slope=par.Slope;
   
            % Errors 
            self.Err_beta=par.Err_beta;
            self.Err_Type=par.Err_Type;
            self.Err_hb=par.Err_hb;
            self.Err_ub=par.Err_ub;
            self.Err_vb=par.Err_vb;

            self.Err_ho=par.Err_ho;
            self.Err_uo=par.Err_uo; 
            self.Err_vo=par.Err_vo;

            self.Inc_beta=par.Inc_beta;
            self.Inc_betaENS=par.Inc_betaENS;
        end
	end
end