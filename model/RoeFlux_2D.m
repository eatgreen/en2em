% Roe flux ============================================================
function F = RoeFlux_2D(hl,hr,ul,ur,vl,vr,flag)

flagX=flag;
flagY=~flag;

F=zeros(size(hl,1),size(hl,2),3);
%initial parameter
g=9.806;

% Roe averages ------------------
duml=hl.^0.5;
dumr=hr.^0.5;

hhat=duml.*dumr;
uhat=(duml.*ul + dumr.*ur)./(duml+dumr);
vhat=(duml.*vl + dumr.*vr)./(duml+dumr);

chat=((0.5*g*(hl+hr)).^0.5);
chatl=((g*hl).^0.5);
chatr=((g*hr).^0.5);

dh=hr-hl;
du=ur-ul;
dv=vr-vl;

uNl = ul.*flagX + vl.*flagY;
uNr = ur.*flagX + vr.*flagY;

% Eigenvalues --------------------
al2=uNl+chatl;
al3=uNl-chatl;
ar2=uNr+chatr;
ar3=uNr-chatr;

a1 = abs(uhat*flagX + vhat*flagY) ;
a2 = abs(uhat*flagX + vhat*flagY + chat);
a3 = abs(uhat*flagX + vhat*flagY - chat);

% Entropy correction?
da2=max(0,2*(ar2-al2));
da3=max(0,2*(ar3-al3));

tmpIndex=a2<da2;
a2(tmpIndex)=0.5*(a2(tmpIndex).*a2(tmpIndex)./da2(tmpIndex)+da2(tmpIndex));

tmpIndex2=a3<da3;
a3(tmpIndex2)=0.5*(a3(tmpIndex2).*a3(tmpIndex2)./da3(tmpIndex2)+da3(tmpIndex2));

% Right eigenvectors ----------------
R21=-flagY;
R22=uhat+flagX*chat;
R23=uhat-flagX*chat;
R31=flagX;
R32=vhat+flagY*chat;
R33=vhat-flagY*chat;

% Wave coefficients x eigenvalues

alpha1 = (hhat.*dv*flagX - hhat.*du*flagY).*a1;
alpha2 = 0.5 * (dh + (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a2;
alpha3 = 0.5 * (dh - (hhat.*du*flagX + hhat.*dv*flagY)./chat).*a3;

%This source term is rather complex
% if strcmp(source_flag,'On')
% Hx=source_term.Hx;
% Hy=source_term.Hy;
%     
% Gx=0.5*g.*(hl+hr).*Hx*flagX;
% Gy=0.5*g.*(hl+hr).*Hy*flagY;
% 
% %Calculate source term in terms of bottom depth variation
% ia1=ones(size(a1))-a1./(uhat*flagX + vhat*flagY);
% ia2=ones(size(a2))-a2./(uhat*flagX + vhat*flagY + chat);
% ia3=ones(size(a3))-a3./(uhat*flagX + vhat*flagY - chat);
% 
% beta1=0.5*(2*Gy*flagX-2*Gx*flagY)./chat.*ia1;
% beta2=0.5*(Gx*flagX+Gy*flagY)./chat.*ia2;
% beta3=0.5*(-Gx*flagX-Gy*flagY)./chat.*ia3;
% 
% S(:,:,1)=beta2+bata3;
% S(:,:,2)=R21.*beta1+R22.*beta2+R23.*beta3;
% S(:,:,3)=R31.*beta1+R32.*beta2+R33.*beta3;
% end

% Left flux
FL1=uNl.*hl;
FL2=ul.*uNl.*hl + 0.5*g.*(hl.*hl)*flagX;
FL3=vl.*uNl.*hl + 0.5*g.*(hl.*hl)*flagY;

% Right flux
FR1=uNr.*hr;
FR2=ur.*uNr.*hr + 0.5*g.*(hr.*hr)*flagX;
FR3=vr.*uNr.*hr + 0.5*g.*(hr.*hr)*flagY;

% Roe flux
F(:,:,1)=0.5*(FL1+FR1-alpha2-alpha3);
F(:,:,2)=0.5*(FL2+FR2-(R21.*alpha1)-(R22.*alpha2)-(R23.*alpha3));
F(:,:,3)=0.5*(FL3+FR3-(R31.*alpha1)-(R32.*alpha2)-(R33.*alpha3));
end