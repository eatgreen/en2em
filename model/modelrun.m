   % ======================================================================
   %                       ***  modelrun  ***
   % 4DENVar : wrapper for calling model
   %======================================================================

   %----------------------------------------------------------------------
   %    modelrun     : input,  initial condition and mode
   %                   output, final condition or trajectory
   %----------------------------------------------------------------------
   
function Xt=modelrun(x0,dym_obj,das_obj,mode)

    option.dynstep=dym_obj.dynstep; option.orderT=dym_obj.orderT; option.BC=dym_obj.BC; 
    switch mode
        case 'synthobs'
            
            option.dim=dym_obj.dim;
            option.X0=x0;
            dt=dym_obj.dtdata;
            dx=dym_obj.dxobs;
            dy=dym_obj.dyobs;
            Xt(:,:,:,1)=x0; 
            t=das_obj.t_init(1); it=1;
            while t<das_obj.tmax-dt/10
                it=it+1; t=t+dt;
                tmp1=foo2(Xt(:,:,:,it-1));
                tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                Xt(:,:,:,it)=foo1(tmp2);
            end
            
        case 'forecast_alone_highres'
            
            option.dim=dym_obj.dim;
            option.X0=x0;
            dt=dym_obj.dtdata;
            dx=dym_obj.dxobs;
            dy=dym_obj.dyobs;
            Xt(:,:,:,1)=x0; 
            t=0;
            tmax=das_obj.t_forecast;
            it=1;
            while t<tmax-dt/10
                it=it+1; t=t+dt;
                tmp1=foo2(Xt(:,:,:,it-1));
                tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                Xt(:,:,:,it)=foo1(tmp2);
            end
                            
        case 'analysis_alone'
            
            option.dim=dym_obj.dim;
            option.locunc=dym_obj.LocUncert_flag;  option.whitenoise=dym_obj.WhiteNoise_flag;
            option.X0=x0;
            dt=dym_obj.dtdata;
            dx=dym_obj.dxobs;
            dy=dym_obj.dyobs;
            Xt(:,:,:,1)=x0; 
            t=das_obj.t_assim_deb(das_obj.CycleIndex); 
            tmax=das_obj.t_assim_fin(das_obj.CycleIndex);
            it=1;
            while t<tmax-dt/10
                it=it+1; t=t+dt;
                tmp1=foo2(Xt(:,:,:,it-1));
                tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                Xt(:,:,:,it)=foo1(tmp2);
            end
                      
        case {'state','forecast','forecast_alone','analysis','ensembleinitial','ensembleintegration','ensembleforecast'}
            
            option.dim=dym_obj.dim;
            option.X0=x0;
            it=1;
            dt=dym_obj.dt;
            dx=dym_obj.dx;
            dy=dym_obj.dy;
            switch mode
                case {'state','forecast','forecast_alone','analysis'}
                    Xt(:,:,:,1)=x0;   
                    switch mode
                        case 'state'
                            t=0; 
                            tmax=das_obj.tmax;
                        case {'forecast','analysis'}
                            t=das_obj.t_assim_deb(das_obj.CycleIndex); 
                            tmax=das_obj.t_assim_fin(das_obj.CycleIndex);
                        case 'forecast_alone'
                            t=0;
                            tmax=das_obj.t_forecast;
                    end
                    while t<tmax-dt/10
                        it=it+1; t=t+dt; 
                        tmp1=foo2(Xt(:,:,:,it-1));
                        tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                        Xt(:,:,:,it)=foo1(tmp2);
                    end
                case 'ensembleforecast'
                    j=1;
                    Xt(:,:,:,1)=x0;
                    t=das_obj.t_assim_deb(das_obj.CycleIndex); 
                    tmax=das_obj.t_assim_fin(das_obj.CycleIndex);
                    while t<tmax-dt/10      
                        tmp1=foo2(x0);
                        tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                        x0=foo1(tmp2);
                        t=t+dt; 
                        if it==dym_obj.t_ratio*j
                            j=j+1;
                            if j<=das_obj.itobs_number
                            Xt(:,:,:,j)=x0;
                            end
                        end
                        it=it+1; 
                    end
                case {'ensembleinitial','ensembleintegration'}
                    switch mode 
                        case 'ensembleinitial'
                            t=0; 
                            tmax=das_obj.t_offset;
                        case 'ensembleintegration'
                            t=0; 
                            tmax=das_obj.tobs_assim;
                    end
                    while t<tmax-dt/10
                        tmp1=foo2(x0);
                        tmp2=TimeIntegration2D(tmp1,dt,dx,dy,option);
                        x0=foo1(tmp2);
                        t=t+dt;
                        it=it+1;  
                    end
                    Xt=x0;
            end
        otherwise
            error('no proper mode Type selected for model run')
    end    
end