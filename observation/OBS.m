   % ======================================================================
   %                       ***  Class OBS  ***
   % 4DENVar : Generic observation class
   %======================================================================

   %----------------------------------------------------------------------
   %    OBS                    : Constructor
   %    make_synth             : make all related synthetic variables
   %    make_initial_condition : Set true initial condition
   %    make_true_solution     : Calculate true trajectory
   %    make_obs               : Calculate observation
   %    make_invR              : Calculate inverse of R
   %    load_obs               : load observation
   %    load_invR              : load inverse of R
   %    make_real              : make all related real variables
   %    make_obsens            : Calculate observation ensemble
   %----------------------------------------------------------------------

classdef OBS < DYM 
    properties(Access=public) 
        x0;             %True Initial condition 
        Xt;             %True trajectory
        Xobs;           %Synthetic observations
        dXobs;          %Synthetic observation perturbations
        Rinv;           %Inverse of R, diagonal
        XobsENS;        %Synthetic observation ensemble
    end
    
    methods
        function self=OBS(par)
            self@DYM(par);
            self.x0=par.x0;
        end
        
        function self=make_synth(self,das_obj,ens_obj)
            self=self.make_true_solution(das_obj);
            self=self.make_obs(das_obj);
            self=self.make_invR(das_obj);
            if strcmp(das_obj.Algo_EnsembleUpdate_Type,'Ensemble_Analysis') || strcmp(das_obj.Algo_EnsembleUpdate_Type,'DEn_Analysis')
                self=self.make_obsens(das_obj,ens_obj);
            end
        end

        function self=make_real(self,das_obj,ens_obj)
            self=self.load_obs();
            self=self.load_invR();
            if strcmp(das_obj.Algo_EnsembleUpdate_Type,'Ensemble_Analysis')
                self=self.make_obsens(das_obj,ens_obj);
            end
        end

        function self=make_true_solution(self,das_obj)
            disp('The synthetic test: true solution') 
            self.Xt=modelrun(self.x0,self,das_obj,'synthobs'); 
        end
        
        function self=make_obs(self,das_obj)
            disp('The synthetic test: make observations') 
            for i=1:das_obj.itobs_sum
                self.dXobs(:,:,1,i)=normrnd(0,self.Err_ho,self.nxobs,self.nyobs);
                self.dXobs(:,:,2,i)=normrnd(0,self.Err_uo,self.nxobs,self.nyobs);
                self.dXobs(:,:,3,i)=normrnd(0,self.Err_vo,self.nxobs,self.nyobs);

                self.Xobs(:,:,:,i)=self.Xt(:,:,:,round(das_obj.t_offset/self.dtdata)+(i-1)*self.t_ratiodata+1)+self.dXobs(:,:,:,i);
            end
                        
            %Thin Xt, time scale mapped to model temporal resolution
            self.Xt=self.Xt(:,:,:,1:self.dt/self.dtdata:end);
            
            Xtrue_rst=self.Xt;
            save('restartfiles/Xt.mat','Xtrue_rst');
            Xobs_rst=self.Xobs;
            save('restartfiles/Xobs.mat','Xobs_rst');
        end
        
        function self=load_obs(self)
            disp('The real test: load observations') 
            var=load('../data/rea/Xobs.mat');
            self.Xobs=var.XobsNew;
            Xobs_rst=self.Xobs;
            save('restartfiles/Xobs.mat','Xobs_rst');
        end
         
        function self=make_invR(self,das_obj)
            disp('The synthetic test: make observation error covariance matrix');
            for i=1:das_obj.itobs_sum
                self.Rinv(:,:,1,i)=(1/self.Err_ho^2)*ones(self.nxobs,self.nyobs);
                self.Rinv(:,:,2,i)=(1/self.Err_uo^2)*ones(self.nxobs,self.nyobs);
                self.Rinv(:,:,3,i)=(1/self.Err_vo^2)*ones(self.nxobs,self.nyobs);
            end
            
            Rinv_rst=self.Rinv;
            save('restartfiles/Rinv.mat','Rinv_rst');
        end

        function self=load_invR(self)
            disp('The real test: load observation error covariance matrix');
            var=load('../data/rea/Rinv.mat');
            self.Rinv=var.Rinv;
        
            Rinv_rst=self.Rinv;
            save('restartfiles/Rinv.mat','Rinv_rst');
        end
        
        function self=make_obsens(self,das_obj,ens_obj)
            disp('The synthetic test: make ensemble observations') 
            for it=1:das_obj.itobs_sum
                Uobs=self.Xobs(:,:,:,it);
                self.XobsENS(:,:,:,ens_obj.nens+1,it)=Uobs;
                for iu=1:ens_obj.nens                   
                    if strcmp(das_obj.Algo_EnsembleUpdate_Type,'Ensemble_Analysis')
                        self.XobsENS(:,:,1,iu,it)=Uobs(:,:,1)+random('norm',0,self.Err_ho,[self.nxobs self.nyobs]);
                        self.XobsENS(:,:,2,iu,it)=Uobs(:,:,2)+random('norm',0,self.Err_uo,[self.nxobs self.nyobs]);
                        self.XobsENS(:,:,3,iu,it)=Uobs(:,:,3)+random('norm',0,self.Err_vo,[self.nxobs self.nyobs]);
                    elseif strcmp(das_obj.Algo_EnsembleUpdate_Type,'DEn_Analysis')
                        self.XobsENS(:,:,1,iu,it)=Uobs(:,:,1);
                        self.XobsENS(:,:,2,iu,it)=Uobs(:,:,2);
                        self.XobsENS(:,:,3,iu,it)=Uobs(:,:,3);
                    else
                        error('algorithm unspecified for creating observation ensemble');
                    end
                end
            end
               
            XobsENS_rst=self.XobsENS;
            save('restartfiles/XobsENS.mat','XobsENS_rst');
        end
        
        function self=load_restartfiles(self,das_obj)
            disp('The synthetic test: load restart files'); 
            
            varXt=load('restartfiles/Xt.mat');
            self.Xt=varXt.Xtrue_rst;
            varXobs=load('restartfiles/Xobs.mat');
            self.Xobs=varXobs.Xobs_rst;
            varRinv=load('restartfiles/Rinv.mat');
            self.Rinv=varRinv.Rinv_rst;            
            if strcmp(das_obj.Algo_EnsembleUpdate_Type,'Ensemble_Analysis') || strcmp(das_obj.Algo_EnsembleUpdate_Type,'DEn_Analysis')
                varXobsENS=load('restartfiles/XobsENS.mat');
                self.XobsENS=varXobsENS.XobsENS_rst;
            end
        end
    end
end