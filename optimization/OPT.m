   % ======================================================================
   %                       ***  Class OPT  ***
   % 4DENVar : Generic optimization class 
   %======================================================================

   %----------------------------------------------------------------------
   %    OPT                  : Constructor 
   %    OptimisationOuter    : Main driver for outer loop
   %    OptimisationInnerSol : Wrapper for single optimization
   %    OptimisationInnerEns : Wrapper for ensemble optimization
   %    OPTINNER             : Main driver for inner loop
   %----------------------------------------------------------------------
   
classdef OPT < handle
    properties(Access=public)
        MaxIter_Outer;
        epsdxx;
        MaxIter_Inner;
        epsg;
        epsx;
        Iter_Outer;
        dZ;
        fval;
        hessian;
        disp_flag;
        dZENS;
   end
    
   methods
        function self=OPT(par)
            self.MaxIter_Outer=par.MaxIter_Outer;
            self.epsdxx=par.epsdxx;
            self.MaxIter_Inner=par.MaxIter_Inner;
            self.epsg=par.epsg;
            self.epsx=par.epsx;
        end
        
        function set.Iter_Outer(self,Iter)
            self.Iter_Outer=Iter;
        end
        
        function set.disp_flag(self,disp_f)
            self.disp_flag=disp_f;
        end
        
        function self=OptimisationOuter(self,dym_obj,das_obj,ens_obj,loc_obj,res_obj)

            %calculate forecast
            das_obj=das_obj.ForecastState(dym_obj);

            %calculate ensemble forecast during the assimilation window
            ens_obj=ens_obj.EnsembleForecast(dym_obj,das_obj,res_obj);

            %reshape initial forecast state for diagnosis purpose
            sfR=reshape(das_obj.sf,[dym_obj.nx*dym_obj.ny*das_obj.augState, 1]);
            tic            
            switch das_obj.Algo_EnsembleUpdate_Type
                case {'Ensemble_Analysis','DEn_Analysis'}

                    %calculate ensemble anomaly matrix
                    ens_obj=ens_obj.EnsembleAnomaly(dym_obj,das_obj);

                    %calculate ensemble innovation
                    ens_obj=ens_obj.EnsembleInnovation(dym_obj,das_obj);
                
                    switch das_obj.Algo_Localization_Type
                    case 'None'

                        delta_Z0  =zeros(ens_obj.nens,ens_obj.nens+1);
                        self.dZENS=zeros(ens_obj.nens,ens_obj.nens+1);

                    case 'Localize_Covariance'

                        delta_Z0  =zeros(ens_obj.nens*loc_obj.r,ens_obj.nens+1);
                        self.dZENS=zeros(ens_obj.nens*loc_obj.r,ens_obj.nens+1);

                        loc_obj=loc_obj.LocalizationConstruct(das_obj,dym_obj);
                        loc_obj=loc_obj.LocalizationCovariance(das_obj,dym_obj,ens_obj);            
                    end

                    self.disp_flag='iter';
                    self=self.OptimisationInnerSol(delta_Z0(:,end),das_obj,dym_obj,ens_obj,loc_obj);
                    self.dZENS(:,end)=self.dZ;
                    
                    self.disp_flag='notify';
                    self=self.OptimisationInnerEns(delta_Z0(:,1:ens_obj.nens),das_obj,dym_obj,ens_obj,loc_obj);

                    switch das_obj.Algo_Localization_Type
                    case 'None'
                        delta_saRENS=ens_obj.HMSbR(:,:,1)*self.dZENS;
                    case 'Localize_Covariance'
                        delta_saRENS=loc_obj.HMPbR(:,:,1)*self.dZENS;
                    end
                       
                    delta_saENS=reshape(delta_saRENS,[dym_obj.nx dym_obj.ny das_obj.augState ens_obj.nens+1]);
                        
                    sa=zeros(dym_obj.nx,dym_obj.ny,5); 

                    sa(:,:,1:3)=das_obj.sf(:,:,1:3)+delta_saENS(:,:,1:3,ens_obj.nens+1);

                    if strcmp(das_obj.ParameterEstimation_flag,'On') 
                        sa(:,:,4)=das_obj.sf(:,:,4)+dym_obj.Inc_beta(das_obj.CycleIndex).*delta_saENS(:,:,4,ens_obj.nens+1);
                        sa(:,:,5)=das_obj.sf(:,:,5)+dym_obj.Inc_beta(das_obj.CycleIndex).*delta_saENS(:,:,5,ens_obj.nens+1);
                    end

                    das_obj.sa=sa;

                    switch das_obj.Algo_EnsembleUpdate_Type
                        case 'Ensemble_Analysis'                           
                            %X0R [:,N+1] contains the ensemble analysis and the analysis.
                            xensa=zeros(dym_obj.nx,dym_obj.ny,3,ens_obj.nens); 
                            xensa(:,:,:,1:ens_obj.nens)=ens_obj.xensf(:,:,:,1:ens_obj.nens)+delta_saENS(:,:,1:3,1:ens_obj.nens);

                        case 'DEn_Analysis'
                            %withdraw background perturbation                      
                            delta_sfENS=reshape(ens_obj.HMSbR(:,:,1),[dym_obj.nx dym_obj.ny das_obj.augState ens_obj.nens]);

                            %X0R [:,N+1] contains the ensemble analysis and the analysis.
                            xensa=zeros(dym_obj.nx,dym_obj.ny,3,ens_obj.nens); 
                            xensa(:,:,:,1:ens_obj.nens)=ens_obj.xensf(:,:,:,1:ens_obj.nens)+0.5*delta_saENS(:,:,1:3,1:ens_obj.nens)+0.5*delta_sfENS(:,:,1:3,1:ens_obj.nens);
                    end
                    
                    ens_obj.xensa=xensa;
                    
                    %-----Determine the convergence criteria
                    if norm(delta_saRENS(1:dym_obj.nx*dym_obj.ny,ens_obj.nens+1),Inf)/norm(sfR(1:dym_obj.nx*dym_obj.ny),Inf) < self.epsdxx
                       disp('Solution converged within MaxIter_Outer');
                    end

                    if self.Iter_Outer==self.MaxIter_Outer
                       disp('Maximum Outer loop iteration reached'); 
                    end
                    
                case 'Ensemble_Transform'
                    %parallelization in terms of grid points
                    lnx=dym_obj.nx;
                    lny=dym_obj.ny;
                    llocalsizex=loc_obj.LocalSizex;
                    llocalsizey=loc_obj.LocalSizey;
                    itobs_num=das_obj.itobs_number;
                    
                    Xobs=dym_obj.Xobs;
                    Rinv=dym_obj.Rinv;
                    
                    RinvSlice=Rinv(:,:,:,das_obj.itobs_deb(das_obj.CycleIndex):das_obj.itobs_fin(das_obj.CycleIndex));

                    lsf=das_obj.sf;
                    lXf=das_obj.Xf;
                    
%                     self.disp_flag='iter';
                    self.disp_flag='notify';
                
                    parfor LocalIndex=1:lnx*lny       
%                     for LocalIndex=1:lnx*lny  %normal for loop for debug      
                        %local index 
                        absiIndex=mod(LocalIndex,lnx);
                        if absiIndex==0
                        absiIndex=lnx;
                        end
                        absjIndex=(LocalIndex-absiIndex)/lnx+1;
                        reliIndex=min(llocalsizex+1,absiIndex);
                        reljIndex=min(llocalsizey+1,absjIndex);

                        %local domain size
                        if absiIndex<llocalsizex+1
                           lx=min(absiIndex+llocalsizex,lnx);
                        elseif absiIndex>lnx-llocalsizex && absiIndex>=llocalsizex+1
                           lx=lnx-absiIndex+llocalsizex+1; 
                        else
                           lx=2*llocalsizex+1;
                        end

                        if absjIndex<llocalsizey+1
                           ly=min(absjIndex+llocalsizey,lny);
                        elseif absjIndex>lny-llocalsizey && absjIndex>=llocalsizey+1
                           ly=lny-absjIndex+llocalsizey+1; 
                        else
                           ly=2*llocalsizey+1;
                        end

                        %local analysis transformation
                        sfmn   =LocalAnalysisOperator(loc_obj,lsf,LocalIndex,dym_obj);
                        Xfmn   =LocalAnalysisOperator(loc_obj,lXf,LocalIndex,dym_obj);
                        HMSbRmn=EnsembleAnomalyLocal(ens_obj,LocalIndex,lx,ly,dym_obj,das_obj,loc_obj);
                        Xobsmn =LocalAnalysisOperator(loc_obj,Xobs,LocalIndex,dym_obj);
                        Rinvmn =LocalAnalysisOperator(loc_obj,RinvSlice,LocalIndex,dym_obj);
                        
                        R_factor_effective=loc_obj.LocalizationObservationFactorEffective(lx,ly,reliIndex,reljIndex); %observation localization, use if needed
                        R_factor_effective=1;
                        
                        for it=1:das_obj.itobs_number
                            Rinvmn(:,:,1,it)=Rinvmn(:,:,1,it).*R_factor_effective;
                            Rinvmn(:,:,2,it)=Rinvmn(:,:,2,it).*R_factor_effective;
                            Rinvmn(:,:,3,it)=Rinvmn(:,:,3,it).*R_factor_effective;
                        end
                        
                        RinvRmn=reshape(Rinvmn(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,:),[],das_obj.itobs_number);
                        VInnomn=squeeze(Xobsmn(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,das_obj.itobs_deb(das_obj.CycleIndex):das_obj.itobs_fin(das_obj.CycleIndex)))-squeeze(Xfmn(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,das_obj.ittru));
                        VInnoRmn=reshape(VInnomn, [], das_obj.itobs_number);
                                                    
                        %call inner loop
                        delta_z0=zeros(ens_obj.nens,1);
                        [dz,~,Hessian]=OPTINNER_hessian(delta_z0,self,itobs_num,VInnoRmn,RinvRmn,HMSbRmn(lx*ly*(das_obj.VarRangeDeb-1)+1:lx*ly*das_obj.VarRangeFin,:,:));

                        delta_saRmn=HMSbRmn(:,:,1)*dz;
                        delta_samn=reshape(delta_saRmn,lx,ly,das_obj.augState);

                        xamn=sfmn(:,:,1:3)+delta_samn(:,:,1:3);

                        [W,S,V]=svd(Hessian);
                        TMmn=W*diag(diag(S).^(-0.5))*V';   

                        SaRmn=HMSbRmn(:,:,1)*TMmn*orth(randn(ens_obj.nens));

                        Samn=reshape(SaRmn,lx,ly,das_obj.augState,ens_obj.nens);

                        xensamn=zeros(lx,ly,3,ens_obj.nens);
                        
                        for k=1:ens_obj.nens
                            xensamn(:,:,:,k)  =xamn+sqrt(ens_obj.nens-1)*Samn(:,:,1:3,k);
                        end

                        delta_saR(LocalIndex,:)=delta_samn(reliIndex,reljIndex,:);
                        xensaR(LocalIndex,:,:)=xensamn(reliIndex,reljIndex,:,:);
                    end

                    delta_sa=reshape(delta_saR,dym_obj.nx,dym_obj.ny,das_obj.augState);
                    sa(:,:,1:3)=das_obj.sf(:,:,1:3)+delta_sa(:,:,1:3);
                    if strcmp(das_obj.ParameterEstimation_flag,'On')
                    sa(:,:,4)=das_obj.sf(:,:,4)+dym_obj.Inc_beta(das_obj.CycleIndex).*delta_sa(:,:,4);
                    sa(:,:,5)=das_obj.sf(:,:,5)+dym_obj.Inc_beta(das_obj.CycleIndex).*delta_sa(:,:,5);
                    end

                    das_obj.sa=sa;

                    xensa=reshape(xensaR,dym_obj.nx,dym_obj.ny,3,ens_obj.nens);  

                    ens_obj.xensa=xensa;

                    %-----Determine the convergence criterail
                    if norm(delta_saR(1:dym_obj.nx*dym_obj.ny,1),Inf)/norm(sfR(1:dym_obj.nx*dym_obj.ny),Inf) < self.epsdxx
                       disp('Solution converged within MaxIter_Outer');
                    end

                    if self.Iter_Outer==self.MaxIter_Outer
                       disp('Maximum Outer loop iteration reached'); 
                    end
            end
            
            if strcmp(ens_obj.EnsembleInflation_flag,'On')
                ens_obj=ens_obj.EnsembleInflation(dym_obj,das_obj);
            end
            
            disp(['Analysis CPU time per iteration: ' num2str(toc)]);
        end
        
        function self=OptimisationInnerSol(self,delta_x0,das_obj,dym_obj,ens_obj,loc_obj)
            Rinv=dym_obj.Rinv;
        
            VInnoR=squeeze(ens_obj.VInnoRENS(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,end,:));
            Rinv=Rinv(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,das_obj.itobs_deb(das_obj.CycleIndex):das_obj.itobs_fin(das_obj.CycleIndex));            
            RinvR=reshape(Rinv,[],das_obj.itobs_number);
            switch das_obj.Algo_Localization_Type
            case 'None'
                HMAbR=ens_obj.HMSbR(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,:,:);
            case 'Localize_Covariance'  
                HMAbR=loc_obj.HMPbR(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,:,:);
            end  

            [self.dZ,self.fval]=OPTINNER(delta_x0,self,das_obj.itobs_number,VInnoR,RinvR,HMAbR);
        end
        
        function self=OptimisationInnerEns(self,delta_X0,das_obj,dym_obj,ens_obj,loc_obj)

            Rinv=dym_obj.Rinv;
            
            VInnoR=squeeze(ens_obj.VInnoRENS(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,:,:));
            Rinv=Rinv(:,:,das_obj.VarRangeDeb:das_obj.VarRangeFin,das_obj.itobs_deb(das_obj.CycleIndex):das_obj.itobs_fin(das_obj.CycleIndex));
            RinvR=reshape(Rinv,[],das_obj.itobs_number);    
            switch das_obj.Algo_Localization_Type
            case 'None'
                HMAbR=ens_obj.HMSbR(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,:,:);
            case 'Localize_Covariance'   
                HMAbR=loc_obj.HMPbR(dym_obj.nx*dym_obj.ny*(das_obj.VarRangeDeb-1)+1:dym_obj.nx*dym_obj.ny*das_obj.VarRangeFin,:,:);
            end    
            
            itobs_num=das_obj.itobs_number;
            parfor i=1:ens_obj.nens
                [dzens(:,i),~]=OPTINNER(delta_X0(:,i),self,itobs_num,squeeze(VInnoR(:,i,:)),RinvR,HMAbR);
            end        
            self.dZENS(:,1:ens_obj.nens)=dzens;
        end
        
        function [dx,fval]=OPTINNER(delta_x0,opt_obj,itobs_number,VInnoR,RinvR,HMAbR)
                        
            lb=-Inf*ones(size(delta_x0)); 
            ub=Inf*ones(size(delta_x0)); 
            size_gradient=size(delta_x0,1);

            options = optimset('MaxIter',opt_obj.MaxIter_Inner,'Algorithm','interior-point',...
                    'Hessian','bfgs','GradObj','on','TolX',opt_obj.epsx,'TolFun',opt_obj.epsdxx,'Display',opt_obj.disp_flag);
                    %'HessUpdate','dfp','PlotFcns',{@optimplotfval,@optimplotfirstorderopt}); 
            options = optimset(options, 'UseParallel', 'always'); 

            [dx,fval,~,~,~,~,~] = fmincon(@nestedfun,delta_x0,[],[],[],[],lb,ub,[],options);

            function [J,G]=nestedfun(delta_x)
                Jo=0;
                Go=zeros(size_gradient,1);
                    for i=1:itobs_number
                        Jo=Jo+0.5.*((HMAbR(:,:,i)*delta_x-VInnoR(:,i)).')*(RinvR(:,i).*(HMAbR(:,:,i)*delta_x-VInnoR(:,i)));
                        Go=Go+HMAbR(:,:,i).'*(RinvR(:,i).*(HMAbR(:,:,i)*delta_x-VInnoR(:,i)));
                    end
                J=0.5.*delta_x.'*delta_x+Jo;
                G=delta_x+Go;
            end
        end

        function [dx,fval,hessian]=OPTINNER_hessian(delta_x0,opt_obj,itobs_number,VInnoR,RinvR,HMAbR)
                        
            lb=-Inf*ones(size(delta_x0)); 
            ub=Inf*ones(size(delta_x0)); 
            size_gradient=size(delta_x0,1);

            options = optimset('MaxIter',opt_obj.MaxIter_Inner,'Algorithm','interior-point',...
                    'GradObj','on','TolX',opt_obj.epsx,'TolFun',opt_obj.epsdxx,'Display',opt_obj.disp_flag);
                    %'Hessian','bfgs','HessUpdate','dfp','PlotFcns',{@optimplotfval,@optimplotfirstorderopt}); 
            options = optimset(options, 'UseParallel', 'always'); 

            [dx,fval,~,~,~,~,hessian] = fmincon(@nestedfun,delta_x0,[],[],[],[],lb,ub,[],options);

            function [J,G,H]=nestedfun(delta_x)
                Jo=0;
                Go=zeros(size_gradient,1);
                Ho=zeros(size_gradient);
                    for i=1:itobs_number
                        Jo=Jo+0.5.*((HMAbR(:,:,i)*delta_x-VInnoR(:,i)).')*(RinvR(:,i).*(HMAbR(:,:,i)*delta_x-VInnoR(:,i)));
                        Go=Go+HMAbR(:,:,i).'*(RinvR(:,i).*(HMAbR(:,:,i)*delta_x-VInnoR(:,i)));
                        Ho=Ho+HMAbR(:,:,i).'*(diag(RinvR(:,i))*HMAbR(:,:,i));
                    end
                J=0.5.*delta_x.'*delta_x+Jo;
                G=delta_x+Go;
                H=eye(size_gradient)+Ho;
            end
        end
   end
end