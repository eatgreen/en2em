   % ======================================================================
   %                       ***  Class PAR  ***
   % 4DENVar : Generic parameter class, preload all paramters
   %======================================================================

   %----------------------------------------------------------------------
   %    PAR           : Constructor value-based parameters
   %    update_par    : Compute derivative parameter
   %----------------------------------------------------------------------
   
classdef PAR < handle
    properties(Access=public)
        Obs_Type;
        IC;
        dim;
		g;
		dynstep;
		fluxType;
		BC;
		Lx;
		Ly;
		nx;
		ny;
		dx;
		dy;
		x;
		y;
		xx;
		yy;
        nstate;
        nsize;
		nxobsdata;
		nyobsdata;
		xobs;
		yobs;
		xxobs;
		yyobs;
		nxobs;
		nyobs;
		dxobs;
		dyobs;
		dt;
		dtdata;
		dtobs;
		t_ratio;
		t_ratiodata;
		orderT;
		Slope;
        Err_Type;
		Err_hb;
		Err_ub;
		Err_vb;
		Err_ho;
		Err_uo;
		Err_vo;
        Err_beta;
        Inc_beta;
        Inc_betaENS;
        CycleIndex;
        Assimilation_cycle_tot;
        Assimilation_cycle;
        restart_flag;
        restart_cycle;
        nobs;
        t_init;
        t_offset;
        forecast_rela_length;
        tobs_assim;
        t_forecast;
        tmax_assim;
        tmax;
        t_assim_deb;
        t_assim_fin;
        itobs_sum;
        itobs_number;
        ittru_sum;
        itobs_deb;
        itobs_fin;
        ittru;
        ittru_assim_deb;
        ittru_assim_fin;
        ittru_number;
        itmax;
        itinit;
        Assimilation_flag;
        VarRangeDeb;
        VarRangeFin;
        augState;
        Assimflag_suffix;
        ParameterEstimation_flag;
        BackgroundState_flag;
        Algo_ControlVector_Type;
        Algo_Precondition_Type;
        Algo_Localization_Type;
        Algo_EnsembleUpdate_Type;
        Localization_flag;
        ObservationPert_flag;
        x0;
        sb;
        RMSXobsXt;
        RMSXbXt;
        RMSXaXt;
        Write_Res_Each_Cycle;
        Localization_Type;
        cod;		    %Cutoff distance
        cod_factor;
        r;              %Truncated number of eigenvalues
        LocalSizex;		%local size x
        LocalSizey;		%local size y
        nens;
        codENS;
        codENS_factor;
        EnsembleType_flag;
        EnType_suffix;
        EnsembleInflation_flag;
        InflationFactor;
        Inf_suffix;
        MaxIter_Outer;
        epsdxx;
        MaxIter_Inner;
        epsg;
        epsx;
        disp_flag;
    end
    
    methods 
        function self=PAR(Obs_Type, IC)
            self.Obs_Type=Obs_Type;
            self.IC=IC;
            switch self.Obs_Type
                case 'Syn2D'
                switch self.IC
                    case 'Wave'
                        %DYM-----------------------------------------------------------------------------------
                        self.g=9.806;
                        self.dim='2D';
                        self.dynstep='dyn';
                        self.BC = 'Reflective'; %Boundary conditions 'Reflective';'Channel';'Dirichlet';'Newmon';                
                        self.fluxType='Roe'; %Flux type to solve the dynamic equations:'Roe';'minmod','composite'; 
                        self.orderT=3;  %order of time scheme

                        %Physical space resolution------
                        self.Lx=6e6; 
                        self.Ly=2.4e6;
                        self.dx=12e4;
                        self.dy=12e4;             

                        %State space size
                        self.nstate=3;

                        %Observation space resolution----- 
                        self.dxobs=12e4; 
                        self.dyobs=12e4;
                        
                        %Temporal space resolution----
                        self.dt=37.5;
                        self.dtdata=37.5; %Observation
                        self.dtobs=150;
                        self.forecast_rela_length=0;
                        
                        % Errors 
                        self.Err_Type='Gaussian';
                        self.Err_beta=1;
                        
                        self.Err_hb=0;
                        self.Err_ub=1;
                        self.Err_vb=0.1;

                        self.Err_ho=1;
                        self.Err_uo=0.01; 
                        self.Err_vo=0.001;

                        %DAS-----------------------------------------------------------------------
                        self.CycleIndex=0;
                        self.Assimilation_cycle_tot=3;
                        self.Assimilation_cycle=3;
                        self.restart_flag='Off';
                        self.restart_cycle=1;
                        self.nobs=3;
                        self.Inc_beta=0.5*ones(self.Assimilation_cycle_tot,1);
                        self.t_init=0;
                     
                        % Flag of Assimilation: 'All';%'Height_only';%'Velocity_only';
                        self.Assimilation_flag='All';
                        
                        self.ParameterEstimation_flag='Off';
                        
                        % Flag of Background state: 'EnsembleMean';'TrueSolution';'Observation';'Theoretical';
                        self.BackgroundState_flag='Theoretical';

                        self.Algo_ControlVector_Type='Ensemble';%'State'
                        self.Algo_Precondition_Type='SquareRootB';%'FullB','FullHessian'
                        self.Algo_Localization_Type='None';%'None';%'Localize_Covariance','Local_Analysis'
                        
                        self.Write_Res_Each_Cycle='Off';

                        %ENS---------------------------------------------------------------------------------
                        self.nens=8;
                        self.codENS=10;

                        self.EnsembleType_flag='Gaussian';
                        self.EnType_suffix='_Gauss';%'_Gauss'

                        self.EnsembleInflation_flag='Off';

                        %OPT--------------------------------------------------------------------------
                        self.MaxIter_Outer=1;
                        self.epsdxx=1e-6;
                        self.MaxIter_Inner=100;
                        self.epsg=1e-6;
                        self.epsx=1e-6;

                    case 'Mug'
                        %DYM--------------------------------------------------------------------------
                        self.g=9.806;
                        self.dim='2D';
                        self.dynstep='dyn';
                        self.BC = 'Reflective'; %Boundary conditions 'Reflective';'Channel';'Dirichlet';'Newmon';                
                        self.fluxType='Roe'; %Flux type to solve the dynamic equations:'Roe';'minmod','composite'; 
                        self.orderT=3;
                        
                        %Physical space resolution-----
                        self.Lx=0.25; 
                        self.Ly=0.1;
                        self.dx=0.01; 
                        self.dy=0.01;
                        
                        %State space size
                        self.nstate=3;

                        %Observation space resolution----- 
                        self.dxobs=self.dx; 
                        self.dyobs=self.dy;

                        %Temporal space resolution----
                        self.dt=0.001;
                        self.dtdata=0.001; %Observation
                        self.dtobs=0.05;
                        self.forecast_rela_length=0;

                        % Errors 
                        self.Err_Type='Gaussian';%'Slope','Gaussian'
                        self.Slope='10%';%'1.3','10%'

                        self.Err_hb=0.005;
                        self.Err_ub=0.001;
                        self.Err_vb=0.001;

                        self.Err_ho=0.00025;
                        self.Err_uo=0.001; 
                        self.Err_vo=0.001;

                        self.Err_beta=0.3;
                        
                        %DAS-----------------------------------------------------------------------
                        self.CycleIndex=0;
                        self.Assimilation_cycle=3;
                        self.nobs=3;
                        self.Inc_beta=0.5*ones(self.Assimilation_cycle,1);

                        self.t_init=0;

                        % Flag of Assimilation: 'All';%'Height_only';%'Velocity_only';
                        self.Assimilation_flag='All';

                        self.ParameterEstimation_flag='Off';
                        
                        % Flag of Background state: 'EnsembleMean';'TrueSolution';'Observation';'Theoretical';
                        self.BackgroundState_flag='Theoretical';

                        self.Algo_ControlVector_Type='Ensemble';%'State'
                        self.Algo_Precondition_Type='SquareRootB';%'FullB','FullHessian'
                        self.Algo_Localization_Type='None';%'None';%'Localize_Covariance','Local_Analysis'
                       
                        self.Write_Res_Each_Cycle='Off';                        

                        %ENS---------------------------------------------------------------------------------
                        self.nens=8;
                        self.codENS=5;

                        self.EnsembleType_flag='Gaussian';%'Timediff';%'Gaussian';%'Parameterian'
                        self.EnType_suffix='_Gauss';%'_Time';%'_Para';%'_Gauss'

                        self.EnsembleInflation_flag='Off';
                        
                        %OPT--------------------------------------------------------------------------
                        self.MaxIter_Outer=1;
                        self.epsdxx=1e-6;
                        self.MaxIter_Inner=100;
                        self.epsg=1e-6;
                        self.epsx=1e-6;

                end
            end
        end
        
        function self=update_par(self)
            %DYM---------------------------------------------------------------------------------
            self.x=0:self.dx:self.Lx+self.dx/4;
            self.y=0:self.dy:self.Ly+self.dy/4;
            [self.xx,self.yy]=meshgrid(self.x,self.y); 
            [self.nx,self.ny]=size(self.xx);

            self.nsize=self.nx*self.ny;

            self.xobs=0:self.dxobs:self.Lx+self.dxobs/4;
            self.yobs=0:self.dyobs:self.Ly+self.dyobs/4;
            [self.xxobs,self.yyobs]=meshgrid(self.xobs,self.yobs); 
            [self.nxobs,self.nyobs]=size(self.xxobs);

            self.t_ratio=self.dtobs/self.dt; %t_ratio is the ratio between dtobs/dt
            self.t_ratiodata=self.dtobs/self.dtdata; %t_ratio is the ratio between dtobs/dt
            
            %DAS---------------------------------------------------------------------------------
            self.tobs_assim=self.dtobs*(self.nobs);
            self.t_forecast=self.forecast_rela_length*self.tobs_assim+(self.Assimilation_cycle_tot-self.Assimilation_cycle)*self.tobs_assim;
            self.tmax_assim=self.t_offset+self.tobs_assim*self.Assimilation_cycle_tot;
            self.tmax=self.t_offset+self.tobs_assim*self.Assimilation_cycle+self.t_forecast;

            self.t_assim_deb=zeros(self.Assimilation_cycle_tot,1);
            self.t_assim_fin=zeros(self.Assimilation_cycle_tot,1);
            for i=1:self.Assimilation_cycle_tot
            self.t_assim_deb(i)=self.t_init+self.t_offset+self.tobs_assim*(i-1);
            end
            self.t_assim_fin=self.t_assim_deb+self.tobs_assim;

            self.itobs_sum=floor((self.tmax_assim-self.t_offset)/self.dtobs);

            self.ittru_sum=floor((self.tmax_assim-self.t_offset)/self.dt)+1;

            self.itobs_number=self.nobs;
            for i=1:self.Assimilation_cycle_tot
            self.itobs_deb(i)=1+self.nobs*(i-1);
            end
            self.itobs_fin=self.itobs_deb+self.nobs-1;

            self.ittru=self.t_ratio*(0:self.nobs-1)+1;

            for i=1:self.Assimilation_cycle_tot
                self.ittru_assim_deb(i)=round(self.t_assim_deb(i)/self.dt)+1;
                self.ittru_number(i)=round(self.tobs_assim/self.dt)+1;
                self.ittru_assim_fin(i)=self.ittru_assim_deb(i)+self.ittru_number(i)-1;
            end

            self.itmax=round(self.tmax/self.dt)+1;
            self.itinit=round(self.t_init/self.dt)+1;

            switch self.Assimilation_flag
            case 'All'
                self.VarRangeDeb=1;
                self.VarRangeFin=3;
                self.Assimflag_suffix='All';
            case 'Height_only'
                self.VarRangeDeb=1;
                self.VarRangeFin=1;
                self.Assimflag_suffix='Hei';
            case 'Velocity_only'
                self.VarRangeDeb=2;
                self.VarRangeFin=3;
                self.Assimflag_suffix='Vel';
            otherwise
                error('No proper assimilation variables set');
            end
            
            if strcmp(self.ParameterEstimation_flag,'Off')
                self.augState=self.nstate;
            else
                disp('Error: Estimating Parameter, make sure you know what you are doing');
                return
            end

            if strcmp(self.Algo_EnsembleUpdate_Type,'Ensemble_Transform')
                assert(strcmp(self.Localization_flag,'On'));
                self.Algo_Localization_Type='Local_Analysis';
            else
                if strcmp(self.Localization_flag,'On')
                    self.Algo_Localization_Type='Localize_Covariance';
                else
                    self.Algo_Localization_Type='None';
                end
            end
            
            if strcmp(self.Algo_EnsembleUpdate_Type,'Ensemble_Analysis')
                self.ObservationPert_flag='On';
            end

            %LOC---------------------------------------------------------------------------------
            self.Localization_Type=self.Algo_Localization_Type;
            self.cod=floor(self.cod_factor*self.ny);
            self.LocalSizex=self.cod;
            self.LocalSizey=self.cod;
            
            %ENS---------------------------------------------------------------------------------
            self.codENS=floor(self.codENS_factor*self.ny);

            if strcmp(self.EnsembleInflation_flag,'On')
            self.Inf_suffix='_Inf';
            else
            self.Inf_suffix='';    
            end
            
            switch self.Obs_Type
                case 'Syn2D'
                    switch self.IC
                        case 'Wave'
                            %Initial condition----------------------------------------------------------------------
                            noise=zeros(self.nxobs, self.nyobs)+pseudo2dM( self.nxobs, self.nyobs, 1, self.codENS, self.codENS, 1, 1, 0 );

                            self.x0(:,:,1)=5000-4e-4*self.xxobs/self.g+self.Err_hb*self.Err_beta*noise;
                            self.x0(:,:,2)=zeros(self.nxobs,self.nyobs);
                            self.x0(:,:,3)=zeros(self.nxobs,self.nyobs);

                            %Background condition------------------------------------------------------------------
                            self.sb(:,:,1)=5000-4e-4*self.xxobs/self.g;
                            self.sb(:,:,2)=zeros(self.nx,self.ny);
                            self.sb(:,:,3)=zeros(self.nx,self.ny);

                        case 'Mug'                            
                            %Initial condition----------------------------------------------------------------------
                            switch self.Err_Type
                                case 'Gaussian'
                                    self.x0(:,:,1)=0.02+0.2*self.xxobs+self.Err_beta*self.Err_hb*pseudo2dM( self.nxobs, self.nyobs, 1, self.codENS, self.codENS, 1, 1, 0 );
                                    self.x0(:,:,2)=zeros(self.nxobs,self.nyobs)+random('norm',0,self.Err_ub,[self.nxobs,self.nyobs]);
                                    self.x0(:,:,3)=zeros(self.nxobs,self.nyobs)+random('norm',0,self.Err_vb,[self.nxobs,self.nyobs]);
                                case 'Slope'
                                    self.x0(:,:,1)=0.02+0.21*self.xxobs+0.1*self.yyobs;
                                    self.x0(:,:,2)=zeros(self.nxobs,self.nyobs)+random('norm',0,self.Err_ub,[self.nxobs,self.nyobs]);
                                    self.x0(:,:,3)=zeros(self.nxobs,self.nyobs)+random('norm',0,self.Err_vb,[self.nxobs,self.nyobs]);
                            end
                            %Background condition------------------------------------------------------------------
                            self.sb(:,:,1)=0.02+0.2*self.xxobs; 
                            self.sb(:,:,2)=zeros(self.nx,self.ny);
                            self.sb(:,:,3)=zeros(self.nx,self.ny);                     
                    end
            end
             
        end
    end
end