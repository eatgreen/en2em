   % ======================================================================
   %                       ***  Class par_config  ***
   % 4DENVar : Generic parameter config class, user defined parameters
   %======================================================================

   %----------------------------------------------------------------------
   %    par_config               : Constructor
   %----------------------------------------------------------------------
   
classdef par_config < PAR
    properties(Access=public)
    end
    methods
        function self=par_config(Obs_Type,IC)
            self@PAR(Obs_Type,IC);

%2D resolution
            self.dx=24e4;
            self.dy=24e4;

            self.dxobs=24e4; 
            self.dyobs=24e4;
           
            self.dt=37.5;
            self.dtdata=37.5; %Observation
            self.dtobs=150;

%common parameters            
            self.Assimilation_cycle_tot=5;
            self.Assimilation_cycle=5;  
            self.restart_flag='Off';
            self.restart_cycle=1;
                        
            %relaxation factor
            self.Err_beta=1;
            
            %number of observations in a cycle
            self.nobs=3;

            %starting assimilation time
            self.t_offset=6000;
            self.forecast_rela_length=0;%9-self.Assimilation_cycle_tot;
            
            %can be 'hei' or 'vel'
            self.Assimilation_flag='All';

            %must be Off in this version
            self.ParameterEstimation_flag='Off';           
            
            self.Write_Res_Each_Cycle='On';

            %'Ensemble_Transform','Ensemble_Analysis','DEn_Analysis'
            %'Ensemble_Transform'->'Localized_Covariance' or 'None'
            %'Ensemble_Analysis' ->'Local_Analysis'
            %Do not try DEn_Analysis unless you know this one
            self.Algo_EnsembleUpdate_Type='Ensemble_Analysis';
            self.Localization_flag='Off';
            self.cod_factor=0.5;
            self.r=15;
            
            self.nens=8;
            self.codENS_factor=0.15;

            self.Err_hb=5;
            self.Err_ub=0.1;
            self.Err_vb=0.1;

            self.EnsembleInflation_flag='Off';
            self.InflationFactor=1.5;
            
            self.Err_ho=0.5;
            self.Err_uo=0.01; 
            self.Err_vo=0.01;
                        
            self.MaxIter_Outer=1;
            
            if self.Assimilation_cycle==1
                self.restart_flag='Off';
                self.restart_cycle=1;
            end
            if strcmp(self.restart_flag,'Off')
                self.restart_cycle=1;
            end
            self=self.update_par;
        end
    end
end