   % ======================================================================
   %                       ***  Class RES  ***
   % 4DENVar : Generic result class for single cycle
   %======================================================================

   %----------------------------------------------------------------------
   %    RES            : Empty Constructor 
   %    ResultCycle    : Prescribe result
   %----------------------------------------------------------------------
   
classdef RES < handle
    properties(Access=public)
        CycleIndex;
        Xobs;
        Xb;
        Xf;
        Xa;
        RMSXbXt;
        RMSXaXt;
        RMSXobsXt;
        xensf;
    end
    
    methods 
        function self=RES()
        end
        
        function self=ResultCycle(self,CycleIndex,Xobs,Xb,Xf,Xa,RMSXbXt,RMSXaXt,RMSXobsXt,xensf)                
            self.CycleIndex=CycleIndex;
            self.Xobs=Xobs;
            self.Xb=Xb;
            self.Xf=Xf;
            self.Xa=Xa;
            self.RMSXbXt=RMSXbXt;
            self.RMSXaXt=RMSXaXt;
            self.RMSXobsXt=RMSXobsXt;
            self.xensf=xensf;
        end
    end
end