function B=BurtFilter(A)

w=[0.05; 0.25; 0.4; 0.25; 0.05];

[nx,ny,it]=size(A);

B=zeros(nx/2,ny/2,it);

for i=1:nx/2
        
    for k=1:5
        B(i,:,:)=B(i,:,:)+w(k)*A(k-3+2*i,:,:);
    end

end

for j=1:ny/2
    
    for k=1:5
        B(:,j,:)=B(:,j,:)+w(k)*B(:,k-3+2*i,:);
    end

end

end