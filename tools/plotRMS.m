function plotRMS

RootDir=pwd;

addpath([RootDir,'/synthobservation/'])
addpath([RootDir,'/model/'])
addpath([RootDir,'/ensemble'])
addpath([RootDir,'/localization'])
addpath([RootDir,'/analysis/'])
addpath([RootDir,'/optimization/'])
addpath([RootDir,'/tools/'])
addpath([RootDir,'/result/'])
addpath([RootDir,'/parameters/'])

Obs_Type='Syn1D';
IC='Wave';
par=par_config(Obs_Type,IC);
dym=DYM(par);
das=DAS(par);

            nvard=dym.nstate-1;
            if strcmp(dym.dim,'2D')
                nvard=dym.nstate;
            end
            
            target1_path='Result/target1';
            target2_path='Result/target2';
            target3_path='Result/target3';
            target4_path='Result/target4';
            target5_path='Result/target5';

            var1=load(strcat(target1_path,'/RMSXobsXtAll.mat'));
%             var2=load(strcat(target2_path,'/RMSXobsXtAll.mat'));
%             var3=load(strcat(target3_path,'/RMSXobsXtAll.mat'));
%             var4=load(strcat(target4_path,'/RMSXobsXtAll.mat'));
%             var5=load(strcat(target5_path,'/RMSXobsXtAll.mat'));

            target1.RMSXobsXtAll=var1.RMSXobsXtAll;
%             target2.RMSXobsXtAll=var2.RMSXobsXtAll;
%             target3.RMSXobsXtAll=var3.RMSXobsXtAll;
%             target4.RMSXobsXtAll=var4.RMSXobsXtAll;
%             target5.RMSXobsXtAll=var5.RMSXobsXtAll;

            var1=load(strcat(target1_path,'/RMSXbXtAll.mat'));
%             var2=load(strcat(target2_path,'/RMSXbXtAll.mat'));
%             var3=load(strcat(target3_path,'/RMSXbXtAll.mat'));
%             var4=load(strcat(target4_path,'/RMSXbXtAll.mat'));
%             var5=load(strcat(target5_path,'/RMSXbXtAll.mat'));

            target1.RMSXbXtAll=var1.RMSXbXtAll;
%             target2.RMSXbXtAll=var2.RMSXbXtAll;
%             target3.RMSXbXtAll=var3.RMSXbXtAll;
%             target4.RMSXbXtAll=var4.RMSXbXtAll;
%             target5.RMSXbXtAll=var5.RMSXbXtAll;
            
            var1=load(strcat(target1_path,'/RMSXaXtAll.mat'));
            var2=load(strcat(target2_path,'/RMSXaXtAll.mat'));
            var3=load(strcat(target3_path,'/RMSXaXtAll.mat'));
            var4=load(strcat(target4_path,'/RMSXaXtAll.mat'));
            var5=load(strcat(target5_path,'/RMSXaXtAll.mat'));
            
            target1.RMSXaXtAll=var1.RMSXaXtAll;
            target2.RMSXaXtAll=var2.RMSXaXtAll;
            target3.RMSXaXtAll=var3.RMSXaXtAll;
            target4.RMSXaXtAll=var4.RMSXaXtAll;
            target5.RMSXaXtAll=var5.RMSXaXtAll;

            to=das.t_offset:dym.dtobs:das.tmax_assim-dym.dtobs;
            t1=das.t_offset:dym.dt:das.tmax;

            f=figure;
            set(f,'name','RMS of Analysis, Background and Obervation w.r.t True solution','numbertitle','off');
            subplot(nvard,1,1);
            plot(t1,target1.RMSXbXtAll(:,1),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Height_only');
            plot(to,target1.RMSXobsXtAll(:,1),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,1),'-k')
            plot(t1,target2.RMSXaXtAll(:,1),'--k');
            plot(t1,target3.RMSXaXtAll(:,1),':k');
            plot(t1,target4.RMSXaXtAll(:,1),'-r');
            plot(t1,target5.RMSXaXtAll(:,1),'--r');
            hold off
            xlabel('t');ylabel('RMSE(h)');

            axis tight

            subplot(nvard,1,2);
            plot(t1,target1.RMSXbXtAll(:,2),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Velocity_only');
            plot(to,target1.RMSXobsXtAll(:,2),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,2),'-k')
            plot(t1,target2.RMSXaXtAll(:,2),'--k');
            plot(t1,target3.RMSXaXtAll(:,2),':k');
            plot(t1,target4.RMSXaXtAll(:,2),'-r');
            plot(t1,target5.RMSXaXtAll(:,2),'--r');
            hold off
            xlabel('t');ylabel('RMSE(u)');
            
            axis tight
                        
            if strcmp(dym.dim,'2D')
            subplot(nvard,1,3);
            plot(t1,target1.RMSXbXtAll(:,3),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Velocity_only');
            plot(to,target1.RMSXobsXtAll(:,3),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,3),'-k')
            plot(t1,target2.RMSXaXtAll(:,3),'--k');
            plot(t1,target3.RMSXaXtAll(:,3),':k');
            plot(t1,target4.RMSXaXtAll(:,3),'-r');
            plot(t1,target5.RMSXaXtAll(:,3),'--r');
            hold off
            xlabel('t');ylabel('RMSE(v)');
            end
            
            axis tight      
end