function plotRMS2D

RootDir=pwd;

addpath([RootDir,'/synthobservation/'])
addpath([RootDir,'/model/'])
addpath([RootDir,'/ensemble'])
addpath([RootDir,'/localization'])
addpath([RootDir,'/analysis/'])
addpath([RootDir,'/optimization/'])
addpath([RootDir,'/tools/'])
addpath([RootDir,'/result/'])
addpath([RootDir,'/parameters/'])

Obs_Type='Syn2D';
IC='Wave';
par=par_config(Obs_Type,IC);
dym=DYM(par);
das=DAS(par);

            nvard=dym.nstate-1;
            
            target1_path='Result/2DLA_N32LOC15_target1/obsAll_nxobs81_nx21_dt37.5_Unc_On_WN_Off_it1_m3_N16_codens10_cod15_r20_toffset6000_AssimCycle5_combo1';
%             target2_path='Result/2DLA_N32LOC5_target2/obsAll_nxobs81_nx21_dt37.5_Unc_On_WN_Off_it2_m3_N32_codens5_locx5_locy5_toffset6000_AssimCycle5_combo1';
            target3_path='Result/2DLA_N32LOC5_target2/obsAll_nxobs81_nx21_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_locx5_locy5_toffset6000_AssimCycle5_combo1_collapse';
            target4_path='Result/2DLA_N128LOC5_target4/obsAll_nxobs81_nx21_dt37.5_Unc_On_WN_Off_it2_m3_N128_codens5_locx5_locy5_toffset6000_AssimCycle5_combo1';
            target5_path='Result/2DLC_N32COD15_target5/obsAll_nxobs81_nx21_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_cod15_r20_toffset6000_AssimCycle5_combo1_bis';

            var1=load(strcat(target1_path,'/RMSXobsXtAll.mat'));
            target1.RMSXobsXtAll=var1.RMSXobsXtAll;

            var1=load(strcat(target1_path,'/RMSXbXtAll.mat'));
            target1.RMSXbXtAll=var1.RMSXbXtAll;
            
            var1=load(strcat(target1_path,'/RMSXaXtAll.mat'));
%             var2=load(strcat(target2_path,'/RMSXaXtAll.mat'));
            var3=load(strcat(target3_path,'/RMSXaXtAll.mat'));
            var4=load(strcat(target4_path,'/RMSXaXtAll.mat'));
            var5=load(strcat(target5_path,'/RMSXaXtAll.mat'));
            
            target1.RMSXaXtAll=var1.RMSXaXtAll;
%             target2.RMSXaXtAll=var2.RMSXaXtAll;
            target3.RMSXaXtAll=var3.RMSXaXtAll;
            target4.RMSXaXtAll=var4.RMSXaXtAll;
            target5.RMSXaXtAll=var5.RMSXaXtAll;

            to=das.t_offset:dym.dtobs:das.tmax_assim-dym.dtobs;
            t1=das.t_offset:dym.dt:das.tmax;

            f=figure;
            set(f,'name','RMS of Analysis, Background and Obervation w.r.t True solution','numbertitle','off');
            subplot(nvard,1,1);
            plot(t1,target1.RMSXbXtAll(:,1),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Height_only');
            plot(to,target1.RMSXobsXtAll(:,1),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,1),'-k')
%             plot(t1,target2.RMSXaXtAll(:,1),'--k');
            plot(t1,target3.RMSXaXtAll(:,1),':k');
            plot(t1,target4.RMSXaXtAll(:,1),'-.k');
            plot(t1,target5.RMSXaXtAll(1:61,1),'-r');
            hold off
            xlabel('t');ylabel('RMSE(h)');

            axis tight

            subplot(nvard,1,2);
            plot(t1,target1.RMSXbXtAll(:,2),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Velocity_only');
            plot(to,target1.RMSXobsXtAll(:,2),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,2),'-k')
%             plot(t1,target2.RMSXaXtAll(:,2),'--k');
            plot(t1,target3.RMSXaXtAll(:,2),':k');
            plot(t1,target4.RMSXaXtAll(:,2),'-.k');
            plot(t1,target5.RMSXaXtAll(1:61,2),'-r');
            hold off
            xlabel('t');ylabel('RMSE(u)');
            
            axis tight
                                    
end