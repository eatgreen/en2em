function plotcombo

RootDir=pwd;

addpath([RootDir,'/synthobservation/'])
addpath([RootDir,'/model/'])
addpath([RootDir,'/ensemble'])
addpath([RootDir,'/localization'])
addpath([RootDir,'/analysis/'])
addpath([RootDir,'/optimization/'])
addpath([RootDir,'/tools/'])
addpath([RootDir,'/result/'])
addpath([RootDir,'/parameters/'])

Obs_Type='Syn1D';
IC='Wave';
par=par_config(Obs_Type,IC);
dym=DYM(par);
das=DAS(par);

            nvard=dym.nstate-1;
            if strcmp(dym.dim,'2D')
                nvard=dym.nstate;
            end
            
            target1_path='Result/combo1/obsAll_nxobs2_nx2_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_cod15_r20_toffset6000_AssimCycle3_combo1';
            target2_path='Result/combo2/obsAll_nxobs2_nx2_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_cod15_r20_toffset6000_AssimCycle3_combo2';
            target3_path='Result/combo3/obsAll_nxobs2_nx2_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_cod15_r20_toffset6000_AssimCycle3_combo3';
            target4_path='Result/combo4/obsAll_nxobs2_nx2_dt37.5_Unc_On_WN_Off_it1_m3_N32_codens10_cod15_r20_toffset6000_AssimCycle3_combo4';
%             target5_path='Result/combo5';

            var1=load(strcat(target1_path,'/RMSXobsXtAll.mat'));
%             var2=load(strcat(target2_path,'/RMSXobsXtAll.mat'));
%             var3=load(strcat(target3_path,'/RMSXobsXtAll.mat'));
%             var4=load(strcat(target4_path,'/RMSXobsXtAll.mat'));
%             var5=load(strcat(target5_path,'/RMSXobsXtAll.mat'));

            target1.RMSXobsXtAll=var1.RMSXobsXtAll;
%             target2.RMSXobsXtAll=var2.RMSXobsXtAll;
%             target3.RMSXobsXtAll=var3.RMSXobsXtAll;
%             target4.RMSXobsXtAll=var4.RMSXobsXtAll;
%             target5.RMSXobsXtAll=var5.RMSXobsXtAll;

            var1=load(strcat(target1_path,'/RMSXbXtAll.mat'));
%             var2=load(strcat(target2_path,'/RMSXbXtAll.mat'));
%             var3=load(strcat(target3_path,'/RMSXbXtAll.mat'));
%             var4=load(strcat(target4_path,'/RMSXbXtAll.mat'));
%             var5=load(strcat(target5_path,'/RMSXbXtAll.mat'));

            target1.RMSXbXtAll=var1.RMSXbXtAll;
%             target2.RMSXbXtAll=var2.RMSXbXtAll;
%             target3.RMSXbXtAll=var3.RMSXbXtAll;
%             target4.RMSXbXtAll=var4.RMSXbXtAll;
%             target5.RMSXbXtAll=var5.RMSXbXtAll;
            
            var1=load(strcat(target1_path,'/RMSXaXtAll.mat'));
            var2=load(strcat(target2_path,'/RMSXaXtAll.mat'));
            var3=load(strcat(target3_path,'/RMSXaXtAll.mat'));
            var4=load(strcat(target4_path,'/RMSXaXtAll.mat'));
%             var5=load(strcat(target5_path,'/RMSXaXtAll.mat'));
            
            target1.RMSXaXtAll=var1.RMSXaXtAll;
            target2.RMSXaXtAll=var2.RMSXaXtAll;
            target3.RMSXaXtAll=var3.RMSXaXtAll;
            target4.RMSXaXtAll=var4.RMSXaXtAll;
%             target5.RMSXaXtAll=var5.RMSXaXtAll;

            trange=1:48*3+1;
            torange=1:3*3;

            to=das.t_offset:dym.dtobs:das.tmax_assim-dym.dtobs;
            t1=das.t_offset:dym.dt:das.tmax;

            f=figure;
            set(f,'name','RMS of Analysis, Background and Obervation w.r.t True solution','numbertitle','off');
            subplot(nvard,1,1);
            plot(t1(trange),target1.RMSXbXtAll(trange,1),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Height_only');
            plot(to(torange),target1.RMSXobsXtAll(torange,1),'bo')
            end
            plot(t1(trange),target1.RMSXaXtAll(trange,1),'-k')
            plot(t1(trange),target2.RMSXaXtAll(trange,1),'--k');
            plot(t1(trange),target3.RMSXaXtAll(trange,1)+1.5,':k');
            plot(t1(trange),target4.RMSXaXtAll(trange,1)+3,'-r');
%             plot(t1(trange),target5.RMSXaXtAll(trange,1),'--r');
            hold off
            xlabel('t');ylabel('RMSE(h)');

            axis tight

            subplot(nvard,1,2);
            plot(t1(trange),target1.RMSXbXtAll(trange,2),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Velocity_only');
            plot(to(torange),target1.RMSXobsXtAll(torange,2),'bo')
            end
            plot(t1(trange),target1.RMSXaXtAll(trange,2),'-k')
            plot(t1(trange),target2.RMSXaXtAll(trange,2),'--k');
            plot(t1(trange),target3.RMSXaXtAll(trange,2)+0.11,':k');
            plot(t1(trange),target4.RMSXaXtAll(trange,2)+0.2,'-r');
%             plot(t1(trange),target5.RMSXaXtAll(trange,2),'--r');
            hold off
            xlabel('t');ylabel('RMSE(u)');
            
            axis tight
                        
            if strcmp(dym.dim,'2D')
            subplot(nvard,1,3);
            plot(t1,target1.RMSXbXtAll(:,3),'-g')
            hold on
            if strcmp(das.Assimilation_flag,'All')||strcmp(das.Assimilation_flag,'Velocity_only');
            plot(to,target1.RMSXobsXtAll(:,3),'bo')
            end
            plot(t1,target1.RMSXaXtAll(:,3),'-k')
            plot(t1,target2.RMSXaXtAll(:,3),'--k');
            plot(t1,target3.RMSXaXtAll(:,3),':k');
            plot(t1,target4.RMSXaXtAll(:,3),'-r');
            plot(t1,target5.RMSXaXtAll(:,3),'--r');
            hold off
            xlabel('t');ylabel('RMS(v)');
            end
            
            axis tight      
end