var=load('fcanom.mat');
fcanom=var.fcanom;
var=load('lcanom.mat');
lcanom=var.lcanom;
var=load('laanom.mat');
laanom=var.laanom;

for n=30

fcanom_mean=mean(mean(fcanom(:,:,n)));    

fcpxx=periodogram(fcanom(:,:,n)-fcanom_mean);
lcpxx=periodogram(lcanom(:,:,n));
lapxx=periodogram(laanom(:,:,n));

figure
subplot(2,2,1)
surf(abs(fcpxx));
axis tight
subplot(2,2,2)
surf(abs(lcpxx));
axis tight
subplot(2,2,3)
surf(abs(lapxx));
axis tight

% fcdft=fft2(fcanom(:,:,n)-fcanom_mean);
% lcdft=fft2(lcanom(:,:,n));
% ladft=fft2(laanom(:,:,n));
% 
% figure
% subplot(2,2,1)
% surf(abs(fcdft));
% axis tight
% subplot(2,2,2)
% surf(abs(lcdft));
% axis tight
% subplot(2,2,3)
% surf(abs(ladft));
% axis tight

end
