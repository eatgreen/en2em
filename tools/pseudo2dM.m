function A = pseudo2dM( n1, n2, lde, rx, ry, dx, dy, theta, random_seed, random_seed2 )
% Generates a spatially-correlated gaussian noise
%  
%  -*- Usage -*-
%    
%    First syntax (recommanded):
%      
%      >> F = pseudo2dM( n1, n2, lde, rx, ry, dx, dy, ?? );
%      
%      Create either a matrix (if lde = 1) of size n1 x n2; either an array (if lde > 1) of size n1 x n2 x lde containing lde random fields of size n1 x n2
%      
%      Parameters:
%      
%        * n1: positive integer, number of rows of the result
%        * n2: positive integer, number of columns of the result
%        * lde: positive integer, number of generated fields
%        * rx: positive float, decorrelation length in x-direction
%        * ry: positive float, decorrelation length in y-direction
%        * dx: positive float, x-resolution of the grid
%        * dy: positive float, y-resolution of the grid
%        * ??: float, angle in degrees (counterclockwise) of the privileged direction
%    
%    Second syntax (backward compatibility):
%      
%      >> F = pseudo2dM( nx, ny, rx, ry, dx, dy, n1, n2, ?? );
%      
%      Create a matrix of size n1 x n2 containing a random field of size n1 x n2
%      
%      Parameters:
%      
%        * nx: positive integer, number of rows of the result
%        * ny: positive integer, number of columns of the result
%        * rx: positive float, decorrelation length in x-direction
%        * ry: positive float, decorrelation length in y-direction
%        * dx: positive float, x-resolution of the grid
%        * dy: positive float, y-resolution of the grid
%        * n1: positive integer, number of rows in the Fourier domain, must be the same as nx
%        * n2: positive integer, number of columns in the Fourier domain, must be the same as ny
%        * ??: float, angle in degrees (counterclockwise) of the privileged direction
%    
%    Remarks:
%      
%      * The direction of the angle depends of the visualisation tool or
%        of your definition: it is counterclockwise if you visualize with
%        pcolor, but clockwise if you visualize with imagesc
%  
%  
%  -*- Example -*-
%    
%    F = pseudo2dM( 256, 256, 1, 3, 3, 1, 1, 0 );
%    figure; h = pcolor( F ); set( h, 'LineStyle', 'none' );
%    
%    F = pseudo2dM( 512, 512, 2, 3, 10, 1, 1, 45 );
%    figure; h = pcolor( F(:,:,1) ); set( h, 'LineStyle', 'none' );
%    figure; h = pcolor( F(:,:,2) ); set( h, 'LineStyle', 'none' );
%  
%  
%  -*- References -*-
%    
%    This method comes from:
%      EVENSEN, Geir. The Ensemble Kalman Filter: theoretical formulation and practical implementation. Ocean Dynamics, 2003, vol. 32, p. 359-360.
%    
%    A FORTRAN reference version is also available on:
%      http://enkf.nersc.no
%    
%    A C version has been created by S??bastien Beyou (INRIA).
%      sebastien.beyou@inria.fr
%    
%    (But both FORTRAN and C versions experience memory problems due to bad interactions with the MATLAB modified version of FFTW.)
%  
%  
%  -*- Authors -*-
%    
%    S??bastien Beyou <sebastien.beyou@inria.fr>
%  
%  
%  -*- History -*-
%    
%    * 2010/02/04: version 1.0 (port from the C version)
%  



% Compatibility with the old function with arguments
%   nx, ny, rx, ry, dx, dy, n1, n2, theta, random_seed
% with nx == n1 and ny == n2, so n1 and n2 are not used
if (nargin == 9 && max(size(random_seed)) == 1) || nargin == 10
    theta = random_seed;
    dy = dx;
    dx = ry;
    ry = rx;
    rx = lde;
    lde = 1;
    if nargin == 10
        random_seed = random_seed2;
    else
        random_seed = rand(n1,n2,1);
    end
elseif nargin == 8
    rng('default');
    random_seed = rand(n1,n2,lde);
end


% Check some parameters
if lde < 1
    error( 'lde < 1' );
end
if rx <= 0
    error( 'rx <= 0' );
end
if ry <= 0
    error( 'ry <= 0' );
end


% Some basic computations

kappa1  = 2*pi / (n1*dx); % Eq. (107)
kappa2 = kappa1*kappa1;
gamma1  = 2*pi / (n2*dy); % Eq. (108)
gamma2 = gamma1*gamma1;


% Resolve the equation (121) of the Evensen's paper

s1 = 3.0/rx; % Initial guess for the root search of Eq. (121)
s2 = 3.0/ry;

[status s1 s2] = newton2d( s1, s2, @eq121, 1e-5, 10, 1, 1.25, 0.25, [n1 n2 kappa1 gamma1 kappa2 gamma2 rx ry] );

if status == 0
    error( 'newton2d did not converged.' );
end


% Coefficients of correlation resulting of the computation of Eq. (121)

halfplane1 = (-floor(n1/2)+1:floor(n1/2)).^2;
halfplane2 = (-floor(n2/2)+1:floor(n2/2)).^2;

coeffampl = sqrt( sum( sum( exp( -2.0*(kappa2*halfplane1'*ones(1,size(halfplane2,2))/(s1*s1) + gamma2*ones(size(halfplane1,2),1)*halfplane2/(s2*s2)) ) ) ) - 1 );

torad = pi/180.0;

a11tmp = 1.0 / (s1*s1);
a22tmp = 1.0 / (s2*s2);
a11 = a11tmp * cos(theta*torad)*cos(theta*torad) + a22tmp * sin(theta*torad)*sin(theta*torad);
a22 = a11tmp * sin(theta*torad)*sin(theta*torad) + a22tmp * cos(theta*torad)*cos(theta*torad);
a12 = 2.0 * (a22tmp-a11tmp) * cos(theta*torad) * sin(theta*torad);


% Final computation of the field in the Fourier domain and un-Fourier-tize

A = zeros(n1,n2,lde);

line1 = [0:floor(n1/2)-1 -n1+floor(n1/2):-1];
line2 = 0:n2-1;
e = exp( -( a11*kappa2*(line1.^2)'*ones(1,size(line2,2)) + a12*kappa1*gamma1*line1'*line2 + a22*gamma2*ones(size(line1,2),1)*(line2.^2) ) ) / coeffampl; 

for k=1:lde
	
    % Prepare the data
    A(:,:,k) = e.*(cos(2*pi*random_seed(:,:,k))+1i*sin(2*pi*random_seed(:,:,k)));
    A(1,1,k) = 0;
	
    % Symmetrize the data
    A(1,floor(n2/2)+2:n2,k) = conj(A(1,floor((n2+1)/2):-1:2,k));
%   A(floor(n1/2)+2:n1,1,k) = conj(A(floor((n1+1)/2):-1:2,1,k));
    A(2:n1,floor(n2/2)+2:n2,k) = conj(A(n1:-1:2,floor((n2+1)/2):-1:2,k));
    
	% Execute the FFT
	A(:,:,k) = ifft2( A(:,:,k), 'symmetric' )*n1*n2;
end

return



% Solve an equation using the Newton's method in 2D
%  
%  @param[in] s1                1st component of sigma, initial guess
%  @param[in] s2                2nd component of sigma, initial guess
%  @param[in] eq(s1,s2,params)  value and derivates of the two functions we search the zeros for
%  @param[in] epsrequired       required precision on the solution (but can be not satisfied if the maximum number of iterations is reached)
%  @param[in] maxiter           number maximum of iterations
%  @param[in] maxattempt        number of attemps of the search which begins on some other (near) place (=1 to inhibit the renewal)
%  @param[in] acceleratorinit   initial value of the accelerator to speed up the convergence (=1 to inhibit the accelerator)
%  @param[in] acceleratorfactor value of an update of the accelerator (=0 to inhibit the update of the accelerator)
%  @return [status s1 s2]       status = 1 if the algorithm converged, 0 else
%                               (s1,s2) nearest value found

function [status s1 s2] = newton2d( s1, s2, eq, epsrequired, maxiter, maxattempt, acceleratorinit, acceleratorfactor, params )

status = 0;%#ok
minerror = inf;
s1min = s1;
s2min = s2;
accelerator = acceleratorinit;

for attempt=1:maxattempt
	
	% Update of the search
	s1 = s1*attempt;
	s2 = s2*attempt;
	accelerator = accelerator-acceleratorfactor/attempt;
		
	% Newton's recurrence
	for iter=1:maxiter
		
		% Calculation of the value and derivates
		[f f1 f2 g g1 g2] = eq( s1, s2, params );
		
		% Updating of the approximated root
		inc1 = (f*g2-f2*g)/(f1*g2-f2*g1);
		inc2 = (f1*g-f*g1)/(f1*g2-f2*g1);
		
		s1 = s1 - accelerator*inc1;
		s2 = s2 - accelerator*inc2;
		
		% To get numbers in ]0,1] (??)
		s1 = max( eps, s1 );
        s2 = max( eps, s2 );
        
        s1 = min( 1, s1 );
        s2 = min( 1, s2 );
		
		% Calculation of the error
		err1 = inc1/(abs(s1)+eps);
		err2 = inc2/(abs(s2)+eps);
		
		% If we find the closest solution
		error = abs(err1) + abs(err2);
		if error < minerror
			
			minerror = error;
			s1min = s1;
			s2min = s2;
		end
		
		% If the Newton's algorithm converges
		if error < epsrequired
			status = 1;
            return
		end
	end
end

s1 = s1min;
s2 = s2min;
status = 0;

return



% Represents the Eq. (121) and its partial derivates in (sigma1,sigma2) which must be annulated in (sigma1,sigma2):
%    \sum_{l=-n1/2+1:n1/2,p=-n2/2+1:n2/2} { exp(-2*((\kappa_l/sigma1)^2+(\gamma_p/sigma2)^2)) * (cos(coeff*z)-exp(-1)) }
%    where (coeff,z) = { (\kappa_l,r_x) when x1-x2=r_x and y1-y2=0  (function f below)
%                      { (\gamma_p,r_y) when x1-x2=0 and y1-y2=r_y  (function g below)

function [f f1 f2 g g1 g2] = eq121( s1, s2, params )

n1 = params(1);
n2 = params(2);
kappa1 = params(3);
gamma1 = params(4);
kappa2 = params(5);
gamma2 = params(6);
rx = params(7);
ry = params(8);

line1 = -floor(n1/2)+1:floor(n1/2);
line2 = -floor(n2/2)+1:floor(n2/2);
sizel1 = size(line1,2);
sizel2 = size(line2,2);

e = exp( -2.0*( (kappa2*(line1/s1).^2)'*ones(1,sizel2) + ones(sizel1,1)*(gamma2*(line2/s2).^2) ) );
e(floor(n1/2),floor(n2/2)) = 0;

coskappa = cos((kappa1*rx*line1)'*ones(1,sizel2)) - exp(-1)*ones(sizel1,sizel2);
f = sum(sum(e.*coskappa));
f1 = sum(sum(e.*((4*kappa2*line1.^2/(s1^3))'*ones(1,sizel2)).*coskappa));
f2 = sum(sum(e.*(ones(sizel1,1)*(4*gamma2*line2.^2/(s2^3))).*coskappa));

cosgamma = cos(ones(sizel1,1)*(gamma1*ry*line2)) - exp(-1)*ones(sizel1,sizel2);
g = sum(sum(e.*cosgamma));
g1 = sum(sum(e.*((4*kappa2*line1.^2/(s1^3))'*ones(1,sizel2)).*cosgamma));
g2 = sum(sum(e.*(ones(sizel1,1)*((4*gamma2*line2.^2)/(s2^3))).*cosgamma));

return
