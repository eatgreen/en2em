function saveforcpp(var,str,ndim)

if ndim==5
    
[nx,ny,nvar,nens,nobs]=size(var);

var_re=reshape(var,nx*ny*nvar*nens,nobs);

elseif ndim==4
    
elseif ndim==3
    
[nx,ny,nvar]=size(var);

var_re=reshape(var,nx*ny*nvar,1);

end

save(str,'var_re','-ascii','-double');

end