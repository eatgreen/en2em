%Visualization

function visu(X)

[nx,ny,it]=size(squeeze(X));
%[nx,ny]=size(X);
% it=1;
x=1:nx;
y=1:ny;

[xx,yy]=ndgrid(x,y);

disp('Visu')

screen=get( 0,'ScreenSize');
figure('position',[0 0 screen(3)/2 screen(4)/2]);

mini=min(min(min(X(:,:,:))));
maxi=max(max(max(X(:,:,:))));

for i=1:50:it
    clf
    hold on
    title('Height display','FontSize',14)
    grid on
    surf(xx,yy,X(:,:,i));
%    surf(xx,yy,X(:,:,i));
%     surf(xx,yy,X(:,:,2,i)./X(:,:,1,i));
%     [cav,tmp]=curl(X(:,:,2,i)./X(:,:,1,i),X(:,:,3,i)./X(:,:,1,i));
%     imagesc(cav);  % Vorticity
%     surf(xx,yy,X(:,:,1,i),cav); 
    axis([0 nx 0 ny mini maxi])
    shading interp
    colorbar
     view(3)
%     xlabel('x','FontSize',14);
%     ylabel('y','FontSize',14);
%     zlabel('Height','FontSize',14);
%     hold off
%     pause
    
end

end